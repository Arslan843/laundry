﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Laundry
{
    public partial class NewCustomer : Form
    { 
        bool cancel;
        SqlConnection Con;
        public NewCustomer()
        {
            InitializeComponent();
            string cs = "Data Source=DESKTOP-NM1RRE;Initial Catalog=Laundry;Integrated Security=True";
            Con = new SqlConnection(cs);
            Con.Open();
        }

        private void buttonCanel_Click(object sender, EventArgs e)
        {
            DialogResult Result = MessageBox.Show("Are You Sure?", "Exit", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
            if (Result == DialogResult.OK)
            {
                this.Close();
            }
            else
            {

            }
        }

        private void buttonRegister_Click(object sender, EventArgs e)
        {
            if(ValidateChildren(ValidationConstraints.Enabled))
            {
                if(string.IsNullOrWhiteSpace(textBoxCnic.Text))
                {
                    cancel = true;
                    textBoxCnic.Focus();
                    errorProviderCnic.SetError(textBoxCnic, "Cnic Is Empty");
                }
                else
                {
                    cancel = false;
                }

                if (string.IsNullOrWhiteSpace(textBoxFirstName.Text))
                {
                    cancel = true;
                    textBoxFirstName.Focus();
                    errorProviderFirstName.SetError(textBoxFirstName, "First Name Is Empty");
                }
                else
                {
                    cancel = false;
                }

                if (string.IsNullOrWhiteSpace(textBoxLastName.Text))
                {
                    cancel = true;
                    textBoxLastName.Focus();
                    errorProviderLastName.SetError(textBoxLastName, "Last Name Is Empty");
                }
                else
                {
                    cancel = false;
                }

                if (string.IsNullOrWhiteSpace(textBoxPhoneNumber.Text))
                {
                    cancel = true;
                    textBoxPhoneNumber.Focus();
                    errorProviderPhoneNumber.SetError(textBoxPhoneNumber, "Phone Number Is Empty");
                }
                else
                {
                    cancel = false;
                }

                if (string.IsNullOrWhiteSpace(textBoxCity.Text))
                {
                    cancel = true;
                    textBoxCity.Focus();
                    errorProviderCity.SetError(textBoxCity, "City Is Empty");
                }
                else
                {
                    cancel = false;
                }

                if (string.IsNullOrWhiteSpace(textBoxAddress.Text))
                {
                    cancel = true;
                    textBoxAddress.Focus();
                    errorProviderAddress.SetError(textBoxAddress, "Address Is Empty");
                }
                else
                {
                    cancel = false;
                }
            }
            try
            {
                if (!(textBoxCnic.Text == "" || textBoxFirstName.Text == "" || textBoxLastName.Text == "" ||
                     textBoxPhoneNumber.Text == "" || textBoxCity.Text == "" || textBoxAddress.Text == ""))
                {
                    SqlCommand cmd = new SqlCommand("sp_NewCustomer", Con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@RegularCustomerCnic", textBoxCnic.Text);
                    cmd.Parameters.AddWithValue("@RegularCustomerFirstName", textBoxFirstName.Text);
                    cmd.Parameters.AddWithValue("@RegularCustomerLastName", textBoxLastName.Text);
                    cmd.Parameters.AddWithValue("@RegularCustomerPhoneNumber", textBoxPhoneNumber.Text);
                    cmd.Parameters.AddWithValue("@RegularCustomerCity", textBoxCity.Text);
                    cmd.Parameters.AddWithValue("@RegularCustomerAddress", textBoxAddress.Text);
                    using (Con)
                    {
                        cmd.ExecuteNonQuery();
                    }
                    MessageBox.Show("Registration Successfull",MessageBoxIcon.Information.ToString());

                    textBoxCnic.Clear();
                    textBoxAddress.Clear();
                    textBoxCity.Clear();
                    textBoxFirstName.Clear();
                    textBoxLastName.Clear();
                    textBoxPhoneNumber.Clear();
                }
                else
                {

                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
