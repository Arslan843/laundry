﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Laundry
{
    public partial class MainPanel : Form
    {
        public MainPanel()
        {
            InitializeComponent();
        }

        private void buttonCanel_Click(object sender, EventArgs e)
        {
           

        }

        private void MainPanel_Load(object sender, EventArgs e)
        {
            labelTime.Font = new Font("DS-Digital", 52 , FontStyle.Bold);
            labelSecond.Font = new Font("DS-Digital", 16, FontStyle.Bold);
            labelDate.Font = new Font("DS-Digital", 18, FontStyle.Bold);
            labelDay.Font = new Font("DS-Digital", 18, FontStyle.Bold);
            timerClock.Start();
        }

        private void timerClock_Tick(object sender, EventArgs e)
        {
            labelTime.Text = DateTime.Now.ToString("HH:mm");
            labelSecond.Text = DateTime.Now.ToString("ss");
            labelDate.Text = DateTime.Now.ToString("MM dd yyyy");
            labelDay.Text = DateTime.Now.ToString("dddd");
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            DialogResult Result = MessageBox.Show("Are You Sure", "Exit", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
            if (Result == DialogResult.OK)
            {
                Application.Exit();
            }
            else
            {

            }
        }

        private void buttonNewCustomer_Click(object sender, EventArgs e)
        {
            NewCustomer Nc = new NewCustomer();
            Nc.Show();
        }

        private void buttonPackage_Click(object sender, EventArgs e)
        {
            NewPackage Np = new NewPackage();
            Np.Show();
        }

        private void buttonorder_Click(object sender, EventArgs e)
        {
            PlaceOrder Pq = new PlaceOrder();
            Pq.Show();
        }

        private void buttonLogSettings_Click(object sender, EventArgs e)
        {
            LoginSetting Ls = new LoginSetting();
            Ls.Show();
        }

        private void buttonSettings_Click(object sender, EventArgs e)
        {
            ItemsSettings Is = new ItemsSettings();
            Is.Show();
        }

        private void buttonRecord_Click(object sender, EventArgs e)
        {
            SalesRecord Sr = new SalesRecord();
            Sr.Show();
        }
    }
}
