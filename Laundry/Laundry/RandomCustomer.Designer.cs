﻿namespace Laundry
{
    partial class RandomCustomer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RandomCustomer));
            this.panel3 = new System.Windows.Forms.Panel();
            this.dateTimePickerDeliveryDate = new System.Windows.Forms.DateTimePicker();
            this.textBoxCustomerAddress = new System.Windows.Forms.TextBox();
            this.labelCustomerAddress = new System.Windows.Forms.Label();
            this.dateTimePickerDate = new System.Windows.Forms.DateTimePicker();
            this.labelSaleDate = new System.Windows.Forms.Label();
            this.textBoxBillNumber = new System.Windows.Forms.TextBox();
            this.labelBillNumber = new System.Windows.Forms.Label();
            this.labelReceiptFooter = new System.Windows.Forms.Label();
            this.labelSelectedItem = new System.Windows.Forms.Label();
            this.textBoxSelectedItem = new System.Windows.Forms.TextBox();
            this.textBoxTotalPrice = new System.Windows.Forms.TextBox();
            this.labelTotalPrice = new System.Windows.Forms.Label();
            this.textBoxPrint = new System.Windows.Forms.TextBox();
            this.buttonPrint = new System.Windows.Forms.Button();
            this.textBoxDiscount = new System.Windows.Forms.TextBox();
            this.labelDiscount = new System.Windows.Forms.Label();
            this.buttonRemove = new System.Windows.Forms.Button();
            this.textBoxCustomerPhone = new System.Windows.Forms.TextBox();
            this.labelCustomerPhone = new System.Windows.Forms.Label();
            this.textBoxCustomerName = new System.Windows.Forms.TextBox();
            this.labelCustomerName = new System.Windows.Forms.Label();
            this.buttonfetch = new System.Windows.Forms.Button();
            this.buttonConfirm = new System.Windows.Forms.Button();
            this.dataGridViewFetched = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewToFetch = new System.Windows.Forms.DataGridView();
            this.selectItem = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ItemId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.buttonCanel = new System.Windows.Forms.Button();
            this.LabelTypeOfCustomer = new System.Windows.Forms.Label();
            this.printDocumentPrint = new System.Drawing.Printing.PrintDocument();
            this.printPreviewDialogView = new System.Windows.Forms.PrintPreviewDialog();
            this.errorProviderBillNo = new System.Windows.Forms.ErrorProvider(this.components);
            this.errorProviderCname = new System.Windows.Forms.ErrorProvider(this.components);
            this.errorProviderCPhone = new System.Windows.Forms.ErrorProvider(this.components);
            this.errorProviderSItem = new System.Windows.Forms.ErrorProvider(this.components);
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFetched)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewToFetch)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderBillNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderCname)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderCPhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderSItem)).BeginInit();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.dateTimePickerDeliveryDate);
            this.panel3.Controls.Add(this.textBoxCustomerAddress);
            this.panel3.Controls.Add(this.labelCustomerAddress);
            this.panel3.Controls.Add(this.dateTimePickerDate);
            this.panel3.Controls.Add(this.labelSaleDate);
            this.panel3.Controls.Add(this.textBoxBillNumber);
            this.panel3.Controls.Add(this.labelBillNumber);
            this.panel3.Controls.Add(this.labelReceiptFooter);
            this.panel3.Controls.Add(this.labelSelectedItem);
            this.panel3.Controls.Add(this.textBoxSelectedItem);
            this.panel3.Controls.Add(this.textBoxTotalPrice);
            this.panel3.Controls.Add(this.labelTotalPrice);
            this.panel3.Controls.Add(this.textBoxPrint);
            this.panel3.Controls.Add(this.buttonPrint);
            this.panel3.Controls.Add(this.textBoxDiscount);
            this.panel3.Controls.Add(this.labelDiscount);
            this.panel3.Controls.Add(this.buttonRemove);
            this.panel3.Controls.Add(this.textBoxCustomerPhone);
            this.panel3.Controls.Add(this.labelCustomerPhone);
            this.panel3.Controls.Add(this.textBoxCustomerName);
            this.panel3.Controls.Add(this.labelCustomerName);
            this.panel3.Controls.Add(this.buttonfetch);
            this.panel3.Controls.Add(this.buttonConfirm);
            this.panel3.Controls.Add(this.dataGridViewFetched);
            this.panel3.Controls.Add(this.dataGridViewToFetch);
            this.panel3.Location = new System.Drawing.Point(12, 56);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(709, 625);
            this.panel3.TabIndex = 24;
            // 
            // dateTimePickerDeliveryDate
            // 
            this.dateTimePickerDeliveryDate.Location = new System.Drawing.Point(230, 310);
            this.dateTimePickerDeliveryDate.Name = "dateTimePickerDeliveryDate";
            this.dateTimePickerDeliveryDate.Size = new System.Drawing.Size(200, 20);
            this.dateTimePickerDeliveryDate.TabIndex = 52;
            this.dateTimePickerDeliveryDate.Value = new System.DateTime(2018, 11, 8, 0, 0, 0, 0);
            // 
            // textBoxCustomerAddress
            // 
            this.textBoxCustomerAddress.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.textBoxCustomerAddress.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCustomerAddress.Location = new System.Drawing.Point(454, 66);
            this.textBoxCustomerAddress.Multiline = true;
            this.textBoxCustomerAddress.Name = "textBoxCustomerAddress";
            this.textBoxCustomerAddress.Size = new System.Drawing.Size(200, 24);
            this.textBoxCustomerAddress.TabIndex = 51;
            // 
            // labelCustomerAddress
            // 
            this.labelCustomerAddress.AutoSize = true;
            this.labelCustomerAddress.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCustomerAddress.ForeColor = System.Drawing.Color.Navy;
            this.labelCustomerAddress.Location = new System.Drawing.Point(454, 48);
            this.labelCustomerAddress.Name = "labelCustomerAddress";
            this.labelCustomerAddress.Size = new System.Drawing.Size(127, 15);
            this.labelCustomerAddress.TabIndex = 50;
            this.labelCustomerAddress.Text = "Customer Address";
            // 
            // dateTimePickerDate
            // 
            this.dateTimePickerDate.Location = new System.Drawing.Point(479, 231);
            this.dateTimePickerDate.Name = "dateTimePickerDate";
            this.dateTimePickerDate.Size = new System.Drawing.Size(200, 20);
            this.dateTimePickerDate.TabIndex = 49;
            this.dateTimePickerDate.Visible = false;
            // 
            // labelSaleDate
            // 
            this.labelSaleDate.AutoSize = true;
            this.labelSaleDate.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSaleDate.ForeColor = System.Drawing.Color.Navy;
            this.labelSaleDate.Location = new System.Drawing.Point(91, 310);
            this.labelSaleDate.Name = "labelSaleDate";
            this.labelSaleDate.Size = new System.Drawing.Size(140, 15);
            this.labelSaleDate.TabIndex = 48;
            this.labelSaleDate.Text = "Select Delivery Date";
            // 
            // textBoxBillNumber
            // 
            this.textBoxBillNumber.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.textBoxBillNumber.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxBillNumber.Location = new System.Drawing.Point(454, 201);
            this.textBoxBillNumber.Multiline = true;
            this.textBoxBillNumber.Name = "textBoxBillNumber";
            this.textBoxBillNumber.Size = new System.Drawing.Size(200, 24);
            this.textBoxBillNumber.TabIndex = 46;
            // 
            // labelBillNumber
            // 
            this.labelBillNumber.AutoSize = true;
            this.labelBillNumber.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBillNumber.ForeColor = System.Drawing.Color.Navy;
            this.labelBillNumber.Location = new System.Drawing.Point(454, 183);
            this.labelBillNumber.Name = "labelBillNumber";
            this.labelBillNumber.Size = new System.Drawing.Size(83, 15);
            this.labelBillNumber.TabIndex = 45;
            this.labelBillNumber.Text = "Bill Number";
            // 
            // labelReceiptFooter
            // 
            this.labelReceiptFooter.AutoSize = true;
            this.labelReceiptFooter.Location = new System.Drawing.Point(282, 312);
            this.labelReceiptFooter.Name = "labelReceiptFooter";
            this.labelReceiptFooter.Size = new System.Drawing.Size(148, 13);
            this.labelReceiptFooter.TabIndex = 42;
            this.labelReceiptFooter.Text = "****Thank You For Coming****";
            this.labelReceiptFooter.Visible = false;
            // 
            // labelSelectedItem
            // 
            this.labelSelectedItem.AutoSize = true;
            this.labelSelectedItem.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSelectedItem.ForeColor = System.Drawing.Color.Navy;
            this.labelSelectedItem.Location = new System.Drawing.Point(454, 138);
            this.labelSelectedItem.Name = "labelSelectedItem";
            this.labelSelectedItem.Size = new System.Drawing.Size(97, 15);
            this.labelSelectedItem.TabIndex = 39;
            this.labelSelectedItem.Text = "Selected Item";
            // 
            // textBoxSelectedItem
            // 
            this.textBoxSelectedItem.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.textBoxSelectedItem.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxSelectedItem.Location = new System.Drawing.Point(457, 156);
            this.textBoxSelectedItem.Multiline = true;
            this.textBoxSelectedItem.Name = "textBoxSelectedItem";
            this.textBoxSelectedItem.Size = new System.Drawing.Size(200, 24);
            this.textBoxSelectedItem.TabIndex = 38;
            // 
            // textBoxTotalPrice
            // 
            this.textBoxTotalPrice.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.textBoxTotalPrice.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxTotalPrice.Location = new System.Drawing.Point(538, 565);
            this.textBoxTotalPrice.Multiline = true;
            this.textBoxTotalPrice.Name = "textBoxTotalPrice";
            this.textBoxTotalPrice.Size = new System.Drawing.Size(100, 22);
            this.textBoxTotalPrice.TabIndex = 37;
            // 
            // labelTotalPrice
            // 
            this.labelTotalPrice.AutoSize = true;
            this.labelTotalPrice.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalPrice.ForeColor = System.Drawing.Color.Navy;
            this.labelTotalPrice.Location = new System.Drawing.Point(454, 568);
            this.labelTotalPrice.Name = "labelTotalPrice";
            this.labelTotalPrice.Size = new System.Drawing.Size(78, 15);
            this.labelTotalPrice.TabIndex = 36;
            this.labelTotalPrice.Text = "Total Price";
            // 
            // textBoxPrint
            // 
            this.textBoxPrint.Font = new System.Drawing.Font("Copperplate Gothic Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPrint.Location = new System.Drawing.Point(454, 231);
            this.textBoxPrint.Multiline = true;
            this.textBoxPrint.Name = "textBoxPrint";
            this.textBoxPrint.Size = new System.Drawing.Size(250, 331);
            this.textBoxPrint.TabIndex = 35;
            // 
            // buttonPrint
            // 
            this.buttonPrint.BackColor = System.Drawing.Color.MidnightBlue;
            this.buttonPrint.Font = new System.Drawing.Font("Copperplate Gothic Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPrint.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.buttonPrint.Location = new System.Drawing.Point(630, 596);
            this.buttonPrint.Name = "buttonPrint";
            this.buttonPrint.Size = new System.Drawing.Size(74, 24);
            this.buttonPrint.TabIndex = 33;
            this.buttonPrint.Text = "Print";
            this.buttonPrint.UseVisualStyleBackColor = false;
            this.buttonPrint.Click += new System.EventHandler(this.buttonPrint_Click);
            // 
            // textBoxDiscount
            // 
            this.textBoxDiscount.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.textBoxDiscount.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxDiscount.Location = new System.Drawing.Point(348, 594);
            this.textBoxDiscount.Multiline = true;
            this.textBoxDiscount.Name = "textBoxDiscount";
            this.textBoxDiscount.Size = new System.Drawing.Size(100, 22);
            this.textBoxDiscount.TabIndex = 18;
            // 
            // labelDiscount
            // 
            this.labelDiscount.AutoSize = true;
            this.labelDiscount.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDiscount.ForeColor = System.Drawing.Color.Navy;
            this.labelDiscount.Location = new System.Drawing.Point(277, 597);
            this.labelDiscount.Name = "labelDiscount";
            this.labelDiscount.Size = new System.Drawing.Size(65, 15);
            this.labelDiscount.TabIndex = 17;
            this.labelDiscount.Text = "Discount";
            // 
            // buttonRemove
            // 
            this.buttonRemove.BackColor = System.Drawing.Color.Navy;
            this.buttonRemove.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonRemove.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonRemove.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12.25F);
            this.buttonRemove.ForeColor = System.Drawing.Color.White;
            this.buttonRemove.Location = new System.Drawing.Point(94, 587);
            this.buttonRemove.Name = "buttonRemove";
            this.buttonRemove.Size = new System.Drawing.Size(85, 33);
            this.buttonRemove.TabIndex = 16;
            this.buttonRemove.Text = "Remove";
            this.buttonRemove.UseVisualStyleBackColor = false;
            this.buttonRemove.Click += new System.EventHandler(this.buttonRemove_Click);
            // 
            // textBoxCustomerPhone
            // 
            this.textBoxCustomerPhone.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.textBoxCustomerPhone.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCustomerPhone.Location = new System.Drawing.Point(457, 111);
            this.textBoxCustomerPhone.Multiline = true;
            this.textBoxCustomerPhone.Name = "textBoxCustomerPhone";
            this.textBoxCustomerPhone.Size = new System.Drawing.Size(200, 24);
            this.textBoxCustomerPhone.TabIndex = 15;
            // 
            // labelCustomerPhone
            // 
            this.labelCustomerPhone.AutoSize = true;
            this.labelCustomerPhone.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCustomerPhone.ForeColor = System.Drawing.Color.Navy;
            this.labelCustomerPhone.Location = new System.Drawing.Point(454, 93);
            this.labelCustomerPhone.Name = "labelCustomerPhone";
            this.labelCustomerPhone.Size = new System.Drawing.Size(115, 15);
            this.labelCustomerPhone.TabIndex = 14;
            this.labelCustomerPhone.Text = "Customer Phone";
            // 
            // textBoxCustomerName
            // 
            this.textBoxCustomerName.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.textBoxCustomerName.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCustomerName.Location = new System.Drawing.Point(454, 21);
            this.textBoxCustomerName.Multiline = true;
            this.textBoxCustomerName.Name = "textBoxCustomerName";
            this.textBoxCustomerName.Size = new System.Drawing.Size(200, 24);
            this.textBoxCustomerName.TabIndex = 13;
            // 
            // labelCustomerName
            // 
            this.labelCustomerName.AutoSize = true;
            this.labelCustomerName.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCustomerName.ForeColor = System.Drawing.Color.Navy;
            this.labelCustomerName.Location = new System.Drawing.Point(451, 3);
            this.labelCustomerName.Name = "labelCustomerName";
            this.labelCustomerName.Size = new System.Drawing.Size(112, 15);
            this.labelCustomerName.TabIndex = 12;
            this.labelCustomerName.Text = "Customer Name";
            // 
            // buttonfetch
            // 
            this.buttonfetch.BackColor = System.Drawing.Color.Navy;
            this.buttonfetch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonfetch.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonfetch.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12.25F);
            this.buttonfetch.ForeColor = System.Drawing.Color.White;
            this.buttonfetch.Location = new System.Drawing.Point(3, 300);
            this.buttonfetch.Name = "buttonfetch";
            this.buttonfetch.Size = new System.Drawing.Size(75, 33);
            this.buttonfetch.TabIndex = 11;
            this.buttonfetch.Text = "Fetch";
            this.buttonfetch.UseVisualStyleBackColor = false;
            this.buttonfetch.Click += new System.EventHandler(this.buttonfetch_Click);
            // 
            // buttonConfirm
            // 
            this.buttonConfirm.BackColor = System.Drawing.Color.Navy;
            this.buttonConfirm.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonConfirm.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonConfirm.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12.25F);
            this.buttonConfirm.ForeColor = System.Drawing.Color.White;
            this.buttonConfirm.Location = new System.Drawing.Point(3, 587);
            this.buttonConfirm.Name = "buttonConfirm";
            this.buttonConfirm.Size = new System.Drawing.Size(85, 33);
            this.buttonConfirm.TabIndex = 10;
            this.buttonConfirm.Text = "Confirm";
            this.buttonConfirm.UseVisualStyleBackColor = false;
            this.buttonConfirm.Click += new System.EventHandler(this.buttonConfirm_Click);
            // 
            // dataGridViewFetched
            // 
            this.dataGridViewFetched.AllowUserToAddRows = false;
            this.dataGridViewFetched.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewFetched.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.Name,
            this.Price,
            this.Quantity});
            this.dataGridViewFetched.Location = new System.Drawing.Point(3, 339);
            this.dataGridViewFetched.Name = "dataGridViewFetched";
            this.dataGridViewFetched.Size = new System.Drawing.Size(445, 242);
            this.dataGridViewFetched.TabIndex = 9;
            this.dataGridViewFetched.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewFetched_CellValueChanged);
            // 
            // Id
            // 
            this.Id.HeaderText = "ItemId";
            this.Id.Name = "Id";
            // 
            // Name
            // 
            this.Name.HeaderText = "ItemName";
            this.Name.Name = "Name";
            // 
            // Price
            // 
            this.Price.HeaderText = "ItemPrice";
            this.Price.Name = "Price";
            // 
            // Quantity
            // 
            this.Quantity.HeaderText = "ItemQuantity";
            this.Quantity.Name = "Quantity";
            // 
            // dataGridViewToFetch
            // 
            this.dataGridViewToFetch.AllowUserToAddRows = false;
            this.dataGridViewToFetch.AllowUserToDeleteRows = false;
            this.dataGridViewToFetch.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewToFetch.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.selectItem,
            this.ItemId,
            this.ItemName,
            this.ItemPrice});
            this.dataGridViewToFetch.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewToFetch.Name = "dataGridViewToFetch";
            this.dataGridViewToFetch.Size = new System.Drawing.Size(445, 291);
            this.dataGridViewToFetch.TabIndex = 8;
            // 
            // selectItem
            // 
            this.selectItem.HeaderText = "Check";
            this.selectItem.Name = "selectItem";
            // 
            // ItemId
            // 
            this.ItemId.HeaderText = "ItemId";
            this.ItemId.Name = "ItemId";
            this.ItemId.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ItemId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ItemName
            // 
            this.ItemName.HeaderText = "ItemName";
            this.ItemName.Name = "ItemName";
            // 
            // ItemPrice
            // 
            this.ItemPrice.HeaderText = "ItemPrice";
            this.ItemPrice.Name = "ItemPrice";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.buttonCanel);
            this.panel2.Controls.Add(this.LabelTypeOfCustomer);
            this.panel2.Location = new System.Drawing.Point(12, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(709, 38);
            this.panel2.TabIndex = 25;
            // 
            // buttonCanel
            // 
            this.buttonCanel.BackColor = System.Drawing.Color.DarkRed;
            this.buttonCanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonCanel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonCanel.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12.25F);
            this.buttonCanel.ForeColor = System.Drawing.Color.White;
            this.buttonCanel.Location = new System.Drawing.Point(675, 4);
            this.buttonCanel.Name = "buttonCanel";
            this.buttonCanel.Size = new System.Drawing.Size(29, 29);
            this.buttonCanel.TabIndex = 7;
            this.buttonCanel.Text = "X";
            this.buttonCanel.UseVisualStyleBackColor = false;
            this.buttonCanel.Click += new System.EventHandler(this.buttonCanel_Click);
            // 
            // LabelTypeOfCustomer
            // 
            this.LabelTypeOfCustomer.AutoSize = true;
            this.LabelTypeOfCustomer.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelTypeOfCustomer.ForeColor = System.Drawing.Color.Navy;
            this.LabelTypeOfCustomer.Location = new System.Drawing.Point(5, 10);
            this.LabelTypeOfCustomer.Name = "LabelTypeOfCustomer";
            this.LabelTypeOfCustomer.Size = new System.Drawing.Size(225, 22);
            this.LabelTypeOfCustomer.TabIndex = 4;
            this.LabelTypeOfCustomer.Text = "For Random Customers";
            // 
            // printDocumentPrint
            // 
            this.printDocumentPrint.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocumentPrint_PrintPage);
            // 
            // printPreviewDialogView
            // 
            this.printPreviewDialogView.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialogView.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialogView.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialogView.Enabled = true;
            this.printPreviewDialogView.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialogView.Icon")));
            this.printPreviewDialogView.Name = "printPreviewDialogView";
            this.printPreviewDialogView.Visible = false;
            // 
            // errorProviderBillNo
            // 
            this.errorProviderBillNo.ContainerControl = this;
            // 
            // errorProviderCname
            // 
            this.errorProviderCname.ContainerControl = this;
            // 
            // errorProviderCPhone
            // 
            this.errorProviderCPhone.ContainerControl = this;
            // 
            // errorProviderSItem
            // 
            this.errorProviderSItem.ContainerControl = this;
            // 
            // RandomCustomer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(733, 693);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFetched)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewToFetch)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderBillNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderCname)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderCPhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderSItem)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button buttonCanel;
        private System.Windows.Forms.Label LabelTypeOfCustomer;
        private System.Windows.Forms.DataGridView dataGridViewToFetch;
        private System.Windows.Forms.DataGridView dataGridViewFetched;
        private System.Windows.Forms.Button buttonfetch;
        private System.Windows.Forms.Button buttonConfirm;
        private System.Windows.Forms.DataGridViewCheckBoxColumn selectItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Price;
        private System.Windows.Forms.DataGridViewTextBoxColumn Quantity;
        private System.Windows.Forms.TextBox textBoxCustomerPhone;
        private System.Windows.Forms.Label labelCustomerPhone;
        private System.Windows.Forms.TextBox textBoxCustomerName;
        private System.Windows.Forms.Label labelCustomerName;
        private System.Windows.Forms.Button buttonRemove;
        private System.Windows.Forms.TextBox textBoxPrint;
        private System.Windows.Forms.Button buttonPrint;
        private System.Windows.Forms.TextBox textBoxDiscount;
        private System.Windows.Forms.Label labelDiscount;
        private System.Windows.Forms.Label labelTotalPrice;
        private System.Windows.Forms.TextBox textBoxTotalPrice;
        private System.Windows.Forms.Label labelSelectedItem;
        private System.Windows.Forms.TextBox textBoxSelectedItem;
        private System.Drawing.Printing.PrintDocument printDocumentPrint;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialogView;
        private System.Windows.Forms.Label labelReceiptFooter;
        private System.Windows.Forms.ErrorProvider errorProviderBillNo;
        private System.Windows.Forms.ErrorProvider errorProviderCname;
        private System.Windows.Forms.ErrorProvider errorProviderCPhone;
        private System.Windows.Forms.ErrorProvider errorProviderSItem;
        private System.Windows.Forms.TextBox textBoxBillNumber;
        private System.Windows.Forms.Label labelBillNumber;
        private System.Windows.Forms.Label labelSaleDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerDate;
        private System.Windows.Forms.Label labelCustomerAddress;
        private System.Windows.Forms.TextBox textBoxCustomerAddress;
        private System.Windows.Forms.DateTimePicker dateTimePickerDeliveryDate;
    }
}