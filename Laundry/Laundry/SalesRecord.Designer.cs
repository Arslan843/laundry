﻿namespace Laundry
{
    partial class SalesRecord
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel2 = new System.Windows.Forms.Panel();
            this.buttonCanel = new System.Windows.Forms.Button();
            this.LabelItemSaleRecords = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelTotalSale = new System.Windows.Forms.Label();
            this.textBoxTotalSale = new System.Windows.Forms.TextBox();
            this.dataGridViewSaleRecord = new System.Windows.Forms.DataGridView();
            this.panelDeleted = new System.Windows.Forms.Panel();
            this.dateTimePickerHistory = new System.Windows.Forms.DateTimePicker();
            this.labelBillHistory = new System.Windows.Forms.Label();
            this.labelMontlyHistory = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.textBoxCustomerName = new System.Windows.Forms.TextBox();
            this.labelCustomerName = new System.Windows.Forms.Label();
            this.labelBill = new System.Windows.Forms.Label();
            this.panelInsert = new System.Windows.Forms.Panel();
            this.labelInserted = new System.Windows.Forms.Label();
            this.labelItemName = new System.Windows.Forms.Label();
            this.textBoxPhoneNumber = new System.Windows.Forms.TextBox();
            this.errorProviderPhoneNumber = new System.Windows.Forms.ErrorProvider(this.components);
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSaleRecord)).BeginInit();
            this.panelDeleted.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panelInsert.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderPhoneNumber)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.buttonCanel);
            this.panel2.Controls.Add(this.LabelItemSaleRecords);
            this.panel2.Location = new System.Drawing.Point(11, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(664, 38);
            this.panel2.TabIndex = 25;
            // 
            // buttonCanel
            // 
            this.buttonCanel.BackColor = System.Drawing.Color.DarkRed;
            this.buttonCanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonCanel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonCanel.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12.25F);
            this.buttonCanel.ForeColor = System.Drawing.Color.White;
            this.buttonCanel.Location = new System.Drawing.Point(625, 3);
            this.buttonCanel.Name = "buttonCanel";
            this.buttonCanel.Size = new System.Drawing.Size(29, 29);
            this.buttonCanel.TabIndex = 7;
            this.buttonCanel.Text = "X";
            this.buttonCanel.UseVisualStyleBackColor = false;
            this.buttonCanel.Click += new System.EventHandler(this.buttonCanel_Click);
            // 
            // LabelItemSaleRecords
            // 
            this.LabelItemSaleRecords.AutoSize = true;
            this.LabelItemSaleRecords.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelItemSaleRecords.ForeColor = System.Drawing.Color.Navy;
            this.LabelItemSaleRecords.Location = new System.Drawing.Point(5, 10);
            this.LabelItemSaleRecords.Name = "LabelItemSaleRecords";
            this.LabelItemSaleRecords.Size = new System.Drawing.Size(142, 22);
            this.LabelItemSaleRecords.TabIndex = 4;
            this.LabelItemSaleRecords.Text = "Sales Records";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.labelTotalSale);
            this.panel1.Controls.Add(this.textBoxTotalSale);
            this.panel1.Controls.Add(this.dataGridViewSaleRecord);
            this.panel1.Controls.Add(this.panelDeleted);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panelInsert);
            this.panel1.Location = new System.Drawing.Point(11, 56);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(664, 477);
            this.panel1.TabIndex = 24;
            // 
            // labelTotalSale
            // 
            this.labelTotalSale.AutoSize = true;
            this.labelTotalSale.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalSale.ForeColor = System.Drawing.Color.Navy;
            this.labelTotalSale.Location = new System.Drawing.Point(405, 445);
            this.labelTotalSale.Name = "labelTotalSale";
            this.labelTotalSale.Size = new System.Drawing.Size(72, 15);
            this.labelTotalSale.TabIndex = 17;
            this.labelTotalSale.Text = "Total Sale";
            // 
            // textBoxTotalSale
            // 
            this.textBoxTotalSale.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.textBoxTotalSale.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxTotalSale.Location = new System.Drawing.Point(483, 442);
            this.textBoxTotalSale.Multiline = true;
            this.textBoxTotalSale.Name = "textBoxTotalSale";
            this.textBoxTotalSale.Size = new System.Drawing.Size(171, 24);
            this.textBoxTotalSale.TabIndex = 17;
            // 
            // dataGridViewSaleRecord
            // 
            this.dataGridViewSaleRecord.AllowUserToAddRows = false;
            this.dataGridViewSaleRecord.AllowUserToDeleteRows = false;
            this.dataGridViewSaleRecord.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewSaleRecord.Location = new System.Drawing.Point(3, 136);
            this.dataGridViewSaleRecord.Name = "dataGridViewSaleRecord";
            this.dataGridViewSaleRecord.ReadOnly = true;
            this.dataGridViewSaleRecord.Size = new System.Drawing.Size(647, 292);
            this.dataGridViewSaleRecord.TabIndex = 24;
            // 
            // panelDeleted
            // 
            this.panelDeleted.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelDeleted.Controls.Add(this.dateTimePickerHistory);
            this.panelDeleted.Controls.Add(this.labelBillHistory);
            this.panelDeleted.Controls.Add(this.labelMontlyHistory);
            this.panelDeleted.Location = new System.Drawing.Point(443, 14);
            this.panelDeleted.Name = "panelDeleted";
            this.panelDeleted.Size = new System.Drawing.Size(211, 97);
            this.panelDeleted.TabIndex = 17;
            // 
            // dateTimePickerHistory
            // 
            this.dateTimePickerHistory.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerHistory.Location = new System.Drawing.Point(6, 55);
            this.dateTimePickerHistory.Name = "dateTimePickerHistory";
            this.dateTimePickerHistory.Size = new System.Drawing.Size(200, 20);
            this.dateTimePickerHistory.TabIndex = 19;
            this.dateTimePickerHistory.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // labelBillHistory
            // 
            this.labelBillHistory.AutoSize = true;
            this.labelBillHistory.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F);
            this.labelBillHistory.ForeColor = System.Drawing.Color.Navy;
            this.labelBillHistory.Location = new System.Drawing.Point(6, 10);
            this.labelBillHistory.Name = "labelBillHistory";
            this.labelBillHistory.Size = new System.Drawing.Size(97, 18);
            this.labelBillHistory.TabIndex = 16;
            this.labelBillHistory.Text = "Bill History ";
            // 
            // labelMontlyHistory
            // 
            this.labelMontlyHistory.AutoSize = true;
            this.labelMontlyHistory.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMontlyHistory.ForeColor = System.Drawing.Color.Navy;
            this.labelMontlyHistory.Location = new System.Drawing.Point(6, 37);
            this.labelMontlyHistory.Name = "labelMontlyHistory";
            this.labelMontlyHistory.Size = new System.Drawing.Size(91, 15);
            this.labelMontlyHistory.TabIndex = 8;
            this.labelMontlyHistory.Text = "Sale Till Now";
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.textBoxCustomerName);
            this.panel3.Controls.Add(this.labelCustomerName);
            this.panel3.Controls.Add(this.labelBill);
            this.panel3.Location = new System.Drawing.Point(226, 14);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(211, 97);
            this.panel3.TabIndex = 17;
            // 
            // textBoxCustomerName
            // 
            this.textBoxCustomerName.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.textBoxCustomerName.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCustomerName.Location = new System.Drawing.Point(6, 55);
            this.textBoxCustomerName.Multiline = true;
            this.textBoxCustomerName.Name = "textBoxCustomerName";
            this.textBoxCustomerName.Size = new System.Drawing.Size(171, 24);
            this.textBoxCustomerName.TabIndex = 18;
            this.textBoxCustomerName.TextChanged += new System.EventHandler(this.textBoxCustomerName_TextChanged);
            // 
            // labelCustomerName
            // 
            this.labelCustomerName.AutoSize = true;
            this.labelCustomerName.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCustomerName.ForeColor = System.Drawing.Color.Navy;
            this.labelCustomerName.Location = new System.Drawing.Point(3, 37);
            this.labelCustomerName.Name = "labelCustomerName";
            this.labelCustomerName.Size = new System.Drawing.Size(122, 15);
            this.labelCustomerName.TabIndex = 17;
            this.labelCustomerName.Text = "Enter Bill Number";
            // 
            // labelBill
            // 
            this.labelBill.AutoSize = true;
            this.labelBill.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F);
            this.labelBill.ForeColor = System.Drawing.Color.Navy;
            this.labelBill.Location = new System.Drawing.Point(3, 10);
            this.labelBill.Name = "labelBill";
            this.labelBill.Size = new System.Drawing.Size(93, 18);
            this.labelBill.TabIndex = 16;
            this.labelBill.Text = "Bill History";
            // 
            // panelInsert
            // 
            this.panelInsert.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelInsert.Controls.Add(this.labelInserted);
            this.panelInsert.Controls.Add(this.labelItemName);
            this.panelInsert.Controls.Add(this.textBoxPhoneNumber);
            this.panelInsert.Location = new System.Drawing.Point(9, 14);
            this.panelInsert.Name = "panelInsert";
            this.panelInsert.Size = new System.Drawing.Size(211, 97);
            this.panelInsert.TabIndex = 16;
            // 
            // labelInserted
            // 
            this.labelInserted.AutoSize = true;
            this.labelInserted.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F);
            this.labelInserted.ForeColor = System.Drawing.Color.Navy;
            this.labelInserted.Location = new System.Drawing.Point(6, 10);
            this.labelInserted.Name = "labelInserted";
            this.labelInserted.Size = new System.Drawing.Size(93, 18);
            this.labelInserted.TabIndex = 16;
            this.labelInserted.Text = "Bill History";
            // 
            // labelItemName
            // 
            this.labelItemName.AutoSize = true;
            this.labelItemName.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelItemName.ForeColor = System.Drawing.Color.Navy;
            this.labelItemName.Location = new System.Drawing.Point(6, 37);
            this.labelItemName.Name = "labelItemName";
            this.labelItemName.Size = new System.Drawing.Size(142, 15);
            this.labelItemName.TabIndex = 8;
            this.labelItemName.Text = "Enter Phone Number";
            // 
            // textBoxPhoneNumber
            // 
            this.textBoxPhoneNumber.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.textBoxPhoneNumber.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPhoneNumber.Location = new System.Drawing.Point(9, 55);
            this.textBoxPhoneNumber.Multiline = true;
            this.textBoxPhoneNumber.Name = "textBoxPhoneNumber";
            this.textBoxPhoneNumber.Size = new System.Drawing.Size(171, 24);
            this.textBoxPhoneNumber.TabIndex = 9;
            this.textBoxPhoneNumber.TextChanged += new System.EventHandler(this.textBoxPhoneNumber_TextChanged);
            // 
            // errorProviderPhoneNumber
            // 
            this.errorProviderPhoneNumber.ContainerControl = this;
            // 
            // SalesRecord
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(688, 539);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "SalesRecord";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SalesRecord";
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSaleRecord)).EndInit();
            this.panelDeleted.ResumeLayout(false);
            this.panelDeleted.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panelInsert.ResumeLayout(false);
            this.panelInsert.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderPhoneNumber)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button buttonCanel;
        private System.Windows.Forms.Label LabelItemSaleRecords;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dataGridViewSaleRecord;
        private System.Windows.Forms.Panel panelDeleted;
        private System.Windows.Forms.Label labelBillHistory;
        private System.Windows.Forms.Label labelMontlyHistory;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox textBoxCustomerName;
        private System.Windows.Forms.Label labelCustomerName;
        private System.Windows.Forms.Label labelBill;
        private System.Windows.Forms.Panel panelInsert;
        private System.Windows.Forms.Label labelInserted;
        private System.Windows.Forms.Label labelItemName;
        private System.Windows.Forms.TextBox textBoxPhoneNumber;
        private System.Windows.Forms.ErrorProvider errorProviderPhoneNumber;
        private System.Windows.Forms.Label labelTotalSale;
        private System.Windows.Forms.TextBox textBoxTotalSale;
        private System.Windows.Forms.DateTimePicker dateTimePickerHistory;
    }
}