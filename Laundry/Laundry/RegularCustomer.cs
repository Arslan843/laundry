﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Laundry
{
    public partial class RegularCustomer : Form
    {
        bool Cancel;
        int TotalPrice = 0;
        int Qty;
        int Price;
        SqlConnection Con;
        public RegularCustomer()
        {
            InitializeComponent();
            string cs = "Data Source=DESKTOP-NM1RRE;Initial Catalog=Laundry;Integrated Security=True";
            Con = new SqlConnection(cs);
            Con.Open();
            loaditem();
            LoadBillNumber();
        }

        private void buttonCanel_Click(object sender, EventArgs e)
        {
            DialogResult Result = MessageBox.Show("Are You Sure?", "Exit", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
            if (Result == DialogResult.OK)
            {
                this.Close();
            }
            else
            {

            }
        }

        private void buttonfetch_Click_1(object sender, EventArgs e)
        {
            foreach (DataGridViewRow item in dataGridViewToFetch.Rows)
            {
                if ((bool)item.Cells[0].Value == true)
                {
                    int n = dataGridViewFetched.Rows.Add();
                    dataGridViewFetched.Rows[n].Cells[0].Value = item.Cells[1].Value.ToString();
                    dataGridViewFetched.Rows[n].Cells[1].Value = item.Cells[2].Value.ToString();
                }
            }
        }

        private void loaditem()
        {
            SqlCommand cmd = new SqlCommand("sp_GetItemData", Con);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            sda.Fill(dt);
            dataGridViewToFetch.Rows.Clear();
            foreach (DataRow item in dt.Rows)
            {
                int n = dataGridViewToFetch.Rows.Add();
                dataGridViewToFetch.Rows[n].Cells[0].Value = false;
                dataGridViewToFetch.Rows[n].Cells[1].Value = item["ItemId"].ToString();
                dataGridViewToFetch.Rows[n].Cells[2].Value = item["ItemName"].ToString();
            }
        }

        private void buttonConfirm_Click(object sender, EventArgs e)
        {
            dataGridViewFetched.Font.Bold.ToString();
            dataGridViewFetched.ForeColor = Color.Maroon;
            textBoxPrint.Clear();
            textBoxPrint.AppendText("\t"+"Hi-Wash Laundry Service " + Environment.NewLine);
            textBoxPrint.AppendText("------------------------------------------------------------------------------------------------------------------------------------------------" + Environment.NewLine);
            textBoxPrint.AppendText("Bill Number : " + textBoxBillNumber.Text + Environment.NewLine);
            textBoxPrint.AppendText("Customer Name : " + textBoxCustomerName.Text +Environment.NewLine);
            textBoxPrint.AppendText("Customer Phone : " + textBoxCustomerPhone.Text + Environment.NewLine);
            textBoxPrint.AppendText("------------------------------------------------------------------------------------------------------------------------------------------------" + Environment.NewLine);
            textBoxPrint.AppendText("  " + "Qty" + "\t" + "Name" + "\t\t" + "Price" + Environment.NewLine);
            for (int i = 0; i < dataGridViewFetched.Rows.Count; i++)
            {
                int totalprice = 0;
                totalprice = totalprice + Convert.ToInt32(textBoxPricePerPiece.Text);
            }
            for (int i = 0; i < dataGridViewFetched.Rows.Count; i++)
            {
                textBoxPrint.AppendText( "  " + dataGridViewFetched.Rows[i].Cells[2].Value + "\t" + dataGridViewFetched.Rows[i].Cells[1].Value + "\t\t" + textBoxPricePerPiece.Text + Environment.NewLine);
            }

            for (int i = 0; i < dataGridViewFetched.Rows.Count; i++)
            {
                textBoxSelectedItem.AppendText(dataGridViewFetched.Rows[i].Cells[2].Value.ToString() + " * " + dataGridViewFetched.Rows[i].Cells[1].Value.ToString() + " - ");
            }
            textBoxPrint.AppendText("------------------------------------------------------------------------------------------------------------------------------------------------" + Environment.NewLine);
            textBoxPrint.AppendText(Convert.ToString("\t" + "Total = " + textBoxTotalPrice.Text + Environment.NewLine));
            textBoxPrint.AppendText("\t" + "Thank You For Visit" + Environment.NewLine);
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewCell Del in dataGridViewFetched.SelectedCells)
            {
                if (Del.Selected)
                {
                    dataGridViewFetched.Rows.RemoveAt(Del.RowIndex);
                    //textBox1.AppendText(Del.ToString());
                    string s = textBoxSelectedItem.Text;
                    if (s.Length > 1)
                    {
                        s = s.Substring(0, s.Length - 10);
                    }
                    else
                    {
                        s = "0";
                    }
                    textBoxSelectedItem.Text = s;
                    TotalPrice = 0;
                    int Qty;
                    int Price;


                    for (int i = 0; i < dataGridViewFetched.Rows.Count; ++i)
                    {
                        Qty = Convert.ToInt32(dataGridViewFetched.Rows[i].Cells[2].Value);
                        Price = Convert.ToInt32(textBoxPricePerPiece.Text);
                        TotalPrice = Qty * Price;

                    }

                    textBoxTotalPrice.Text = TotalPrice.ToString();
                    textBoxPrint.Clear();
                    textBoxPrint.AppendText("------------------------------------------------------------------------------------------------------------------------" + Environment.NewLine);
                    textBoxPrint.AppendText("  " + "Qty" + "\t" + "Name" + "\t\t" + "Price" + Environment.NewLine);
                    for (int i = 0; i < dataGridViewFetched.Rows.Count; i++)
                    {
                        textBoxPrint.AppendText("  " + dataGridViewFetched.Rows[i].Cells[2].Value + "\t" + dataGridViewFetched.Rows[i].Cells[1].Value + "\t\t" + textBoxPricePerPiece.Text + Environment.NewLine);
                    }
                }
            }
        }

        private void textBoxCustomerId_TextChanged(object sender, EventArgs e)
        {
            SqlCommand cmd = new SqlCommand("sp_GetRegularCustomer", Con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CustomerId", textBoxCustomerId.Text);
            DataTable dt = new DataTable();
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            sda.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                textBoxCustomerPhone.Text = dt.Rows[0]["RegularCustomerPhoneNumber"].ToString();
                textBoxPricePerPiece.Text = dt.Rows[0]["PackagesPricePerPiece"].ToString();
                textBoxCustomerName.Text = dt.Rows[0]["RegularCustomerFirstName"].ToString() +" "+ dt.Rows[0]["RegularCustomerLastName"].ToString();
            }
            else
            {
                MessageBox.Show("Enter Invalid Id", MessageBoxIcon.Information.ToString());
            }
        }

        private void dataGridViewFetched_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            TotalPrice = 0;
            int Qty;
            int Price;
            for (int i = 0; i < dataGridViewFetched.Rows.Count; ++i)
            {
                Qty = Convert.ToInt32(dataGridViewFetched.Rows[i].Cells[2].Value);
                Price = Convert.ToInt32(textBoxPricePerPiece.Text);
                TotalPrice += Qty * Price;
            }
            textBoxTotalPrice.Text = TotalPrice.ToString();
        }

        private void printDocumentPrint_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            Bitmap BitmapObj = new Bitmap(this.textBoxPrint.Width, this.textBoxPrint.Height);
            textBoxPrint.DrawToBitmap(BitmapObj, new Rectangle(0, 0, this.textBoxPrint.Width, this.textBoxPrint.Height));
            e.Graphics.DrawImage(BitmapObj, 250, 90);
            //e.Graphics.DrawString(labelHiWashHeader.Text, new Font("verdana", 20, FontStyle.Bold), Brushes.Maroon, new Point(270, 20));
            //e.Graphics.DrawString(labelReciept.Text, new Font("verdana", 18, FontStyle.Bold), Brushes.Maroon, new Point(360, 50));
            //e.Graphics.DrawString(labelReceiptFooter.Text, new Font("verdana", 18, FontStyle.Bold), Brushes.Maroon, new Point(200, 500));
            //e.Graphics.DrawString(labelTp.Text, new Font("verdana", 16, FontStyle.Bold), Brushes.Maroon, new Point(350, 450));
        }

        private void buttonPrint_Click(object sender, EventArgs e)
        {
            if (ValidateChildren(ValidationConstraints.Enabled))
            {
                if (string.IsNullOrWhiteSpace(textBoxCustomerId.Text))
                {
                    Cancel = true;
                    textBoxCustomerId.Focus();
                    errorProviderCId.SetError(textBoxCustomerId, "Customer Id is Empty");
                }
                else
                {
                    Cancel = false;
                }

                if (string.IsNullOrWhiteSpace(textBoxCustomerPhone.Text))
                {
                    Cancel = true;
                    textBoxCustomerPhone.Focus();
                    errorProviderCPhone.SetError(textBoxCustomerPhone, "Customer Phone Number is Empty");
                }
                else
                {
                    Cancel = false;
                }

                if (string.IsNullOrWhiteSpace(textBoxSelectedItem.Text))
                {
                    Cancel = true;
                    textBoxSelectedItem.Focus();
                    errorProviderSelectedItem.SetError(textBoxSelectedItem, "Selected Items is Empty");
                }
                else
                {
                    Cancel = false;
                }

                if (string.IsNullOrWhiteSpace(textBoxPricePerPiece.Text))
                {
                    Cancel = true;
                    textBoxPricePerPiece.Focus();
                    errorProviderSelectedItem.SetError(textBoxPricePerPiece, "Selected Items is Empty");
                }
                else
                {
                    Cancel = false;
                }

                if (!(textBoxSelectedItem.Text == "" || textBoxPricePerPiece.Text == "" ||
                    textBoxCustomerId.Text == "" || textBoxCustomerPhone.Text == ""))
                {
                    try
                    {
                        SqlCommand cmd = new SqlCommand("sp_SaveRegularCustomerRecord", Con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@CustomerPhone", textBoxCustomerPhone.Text);
                        cmd.Parameters.AddWithValue("@Items", textBoxSelectedItem.Text);
                        cmd.Parameters.AddWithValue("@TotalAmount", textBoxTotalPrice.Text);
                        cmd.Parameters.AddWithValue("@SaleDate", DateTime.Now.Date.ToShortDateString().ToString());
                        using (Con)
                        {
                            cmd.ExecuteNonQuery();
                        }
                        MessageBox.Show("Inserted Successfully", MessageBoxIcon.Information.ToString());
                        //labelTp.Text = Convert.ToString("Price = " + textBoxTotalPrice.Text);
                        printPreviewDialogView.Document = printDocumentPrint;
                        printPreviewDialogView.ShowDialog();
                        textBoxCustomerPhone.Clear();
                        textBoxSelectedItem.Clear();
                        textBoxPricePerPiece.Clear();
                    }
                    catch(Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
            }
        }
        private void LoadBillNumber()
        {
            SqlCommand cmd = new SqlCommand("SELECT MAX(BillId) + 1 as BillId FROM Sales", Con);
            using (SqlDataReader sdr = cmd.ExecuteReader())
            {
                if (sdr.Read())
                {
                    textBoxBillNumber.Text = sdr["BillId"].ToString();
                }
                sdr.Close();
            }

        }
    }
}
