﻿namespace Laundry
{
    partial class Loader
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Loader));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.LoaderProgress1 = new System.Windows.Forms.Panel();
            this.LabelVersion = new System.Windows.Forms.Label();
            this.LoaderProgress2 = new System.Windows.Forms.Panel();
            this.timerLoader = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.LoaderProgress1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(-7, -1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(526, 234);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // LoaderProgress1
            // 
            this.LoaderProgress1.BackColor = System.Drawing.Color.YellowGreen;
            this.LoaderProgress1.Controls.Add(this.LabelVersion);
            this.LoaderProgress1.Location = new System.Drawing.Point(1, 229);
            this.LoaderProgress1.Name = "LoaderProgress1";
            this.LoaderProgress1.Size = new System.Drawing.Size(511, 24);
            this.LoaderProgress1.TabIndex = 1;
            // 
            // LabelVersion
            // 
            this.LabelVersion.AutoSize = true;
            this.LabelVersion.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelVersion.ForeColor = System.Drawing.Color.Transparent;
            this.LabelVersion.Location = new System.Drawing.Point(126, 2);
            this.LabelVersion.Name = "LabelVersion";
            this.LabelVersion.Size = new System.Drawing.Size(239, 22);
            this.LabelVersion.TabIndex = 3;
            this.LabelVersion.Text = "Smart Laundry App V 1.0";
            // 
            // LoaderProgress2
            // 
            this.LoaderProgress2.BackColor = System.Drawing.Color.MidnightBlue;
            this.LoaderProgress2.Location = new System.Drawing.Point(1, 229);
            this.LoaderProgress2.Name = "LoaderProgress2";
            this.LoaderProgress2.Size = new System.Drawing.Size(20, 24);
            this.LoaderProgress2.TabIndex = 2;
            // 
            // timerLoader
            // 
            this.timerLoader.Enabled = true;
            this.timerLoader.Interval = 25;
            this.timerLoader.Tick += new System.EventHandler(this.timerLoader_Tick);
            // 
            // Loader
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(515, 258);
            this.Controls.Add(this.LoaderProgress2);
            this.Controls.Add(this.LoaderProgress1);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Loader";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Loading";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.LoaderProgress1.ResumeLayout(false);
            this.LoaderProgress1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel LoaderProgress1;
        private System.Windows.Forms.Panel LoaderProgress2;
        private System.Windows.Forms.Timer timerLoader;
        private System.Windows.Forms.Label LabelVersion;
    }
}

