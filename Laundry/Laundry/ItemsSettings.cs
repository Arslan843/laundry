﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Laundry
{
    public partial class ItemsSettings : Form
    {
        bool Cancel;
        SqlConnection Con;
        ConnectionState State = ConnectionState.Open;
        public ItemsSettings()
        {
            InitializeComponent();
            string cs = "Data Source=DESKTOP-NM1RRE;Initial Catalog=Laundry;Integrated Security=True";
            Con = new SqlConnection(cs);

            dataGridViewItemRecord.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }

        private void buttonCanel_Click(object sender, EventArgs e)
        {
            DialogResult Result = MessageBox.Show("Are You Sure?", "Exit", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
            if (Result == DialogResult.OK)
            {
                this.Close();
            }
            else
            {

            }
        }

        private void dataGridViewItemRecord_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridViewItemRecord.Rows.Count > 0)
            {
                textBoxUId.Text = dataGridViewItemRecord.SelectedRows[0].Cells[0].Value.ToString();
                textBoxUName.Text = dataGridViewItemRecord.SelectedRows[0].Cells[1].Value.ToString();
                textBoxUPrice.Text = dataGridViewItemRecord.SelectedRows[0].Cells[2].Value.ToString();
                textBoxDeleteId.Text = dataGridViewItemRecord.SelectedRows[0].Cells[0].Value.ToString();
            }
        }

        private void buttonInsert_Click(object sender, EventArgs e)
        {
            if (ValidateChildren(ValidationConstraints.Enabled))
            {
                if (string.IsNullOrWhiteSpace(textBoxItemName.Text))
                {
                    Cancel = true;
                    textBoxItemName.Focus();
                    errorProviderInsertItemName.SetError(textBoxItemName, "Name Is Empty");
                }
                else
                {
                    Cancel = false;
                }

                if (string.IsNullOrWhiteSpace(textBoxItemPrice.Text))
                {
                    Cancel = true;
                    textBoxItemPrice.Focus();
                    errorProviderInsertItemPrice.SetError(textBoxItemPrice, "Price Is Empty");
                }
                else
                {
                    Cancel = false;
                }
                try
                {
                    if (!(textBoxItemName.Text == "" || textBoxItemPrice.Text == ""))
                    {
                        SqlCommand cmd = new SqlCommand("sp_InsertItem", Con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ItemName", textBoxItemName.Text);
                        cmd.Parameters.AddWithValue("@ItemPrice", textBoxItemPrice.Text);
                        Con.Open();
                            cmd.ExecuteNonQuery();
                        Con.Close();
                        MessageBox.Show("Inserted Successfull", MessageBoxIcon.Information.ToString());
                        GetItemData();
                        textBoxItemName.Clear();
                        textBoxItemPrice.Clear();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

            }
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            UpdateItems();
        }

        private void UpdateItems()
        {
            if (ValidateChildren(ValidationConstraints.Enabled))
            {
                if (string.IsNullOrWhiteSpace(textBoxUId.Text))
                {
                    Cancel = true;
                    textBoxUId.Focus();
                    errorProviderUpdateId.SetError(textBoxUId, "Id Is Empty");
                }
                else
                {
                    Cancel = false;
                }

                if (string.IsNullOrWhiteSpace(textBoxUName.Text))
                {
                    Cancel = true;
                    textBoxUName.Focus();
                    errorProviderUpdateName.SetError(textBoxUName, "Name Is Empty");
                }
                else
                {
                    Cancel = false;
                }

                if (string.IsNullOrWhiteSpace(textBoxUPrice.Text))
                {
                    Cancel = true;
                    textBoxUPrice.Focus();
                    errorProviderItemPrice.SetError(textBoxUPrice, "Price Is Empty");
                }
                else
                {
                    Cancel = false;
                }

                try
                {;
                    if (!(textBoxUId.Text == "" || textBoxUName.Text == "" || textBoxUPrice.Text == ""))
                    {
                        SqlCommand cmd = new SqlCommand("sp_UpdateItem", Con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ItemId", textBoxUId.Text);
                        cmd.Parameters.AddWithValue("@ItemName", textBoxUName.Text);
                        cmd.Parameters.AddWithValue("@ItemPrice", textBoxUPrice.Text);
                        Con.Open();
                            cmd.ExecuteNonQuery();
                        Con.Close();
                        MessageBox.Show("Updated Successfull", MessageBoxIcon.Information.ToString());
                        GetItemData();
                        textBoxUId.Clear();
                        textBoxUName.Clear();
                        textBoxUPrice.Clear();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void buttonDeleted_Click(object sender, EventArgs e)
        {
            DeleteItems();
        }

        private void DeleteItems()
        {
            if (ValidateChildren(ValidationConstraints.Enabled))
            {
                if (string.IsNullOrWhiteSpace(textBoxDeleteId.Text))
                {
                    Cancel = true;
                    textBoxDeleteId.Focus();
                    errorProviderDeleteid.SetError(textBoxDeleteId, "Id Is Empty");
                }
                else
                {
                    Cancel = false;
                }

                try
                {
                    if (!(textBoxDeleteId.Text == ""))
                    {
                        SqlCommand cmd = new SqlCommand("sp_DeleteItem", Con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ItemId", textBoxDeleteId.Text);
                        Con.Open();
                            cmd.ExecuteNonQuery();
                        Con.Close();
                        MessageBox.Show("Delete Successfull", MessageBoxIcon.Information.ToString());
                        GetItemData();
                        textBoxDeleteId.Clear();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }

        }
        private void GetItemData()
        {
            SqlCommand cmd = new SqlCommand("sp_GetItemData", Con);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            sda.Fill(dt);
            dataGridViewItemRecord.DataSource = dt;
        }

        private void ItemsSettings_Load(object sender, EventArgs e)
        {
            GetItemData();
        }
    }
}
