﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Laundry
{
    public partial class RandomCustomer : Form
    {
        SqlConnection Con;
        int TotalPrice = 0;
        bool Cancel;
        public RandomCustomer()
        {
            InitializeComponent();
            string cs = "Data Source=DESKTOP-NM1RRE;Initial Catalog=Laundry;Integrated Security=True";
            Con = new SqlConnection(cs);
            Con.Open();
            Loaditems();
            LoadBillNumber();
        }

        private void buttonCanel_Click(object sender, EventArgs e)
        {
            DialogResult Result = MessageBox.Show("Are You Sure?", "Exit", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
            if (Result == DialogResult.OK)
            {
                this.Close();
            }
            else
            {

            }
        }

        private void Loaditems()
        {
            SqlCommand cmd = new SqlCommand("sp_GetItemData",Con);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            sda.Fill(dt);
            dataGridViewToFetch.Rows.Clear();
            foreach (DataRow item in dt.Rows)
            {
                int n = dataGridViewToFetch.Rows.Add();
                dataGridViewToFetch.Rows[n].Cells[0].Value = false;
                dataGridViewToFetch.Rows[n].Cells[1].Value = item["ItemId"].ToString();
                dataGridViewToFetch.Rows[n].Cells[2].Value = item["ItemName"].ToString();
                dataGridViewToFetch.Rows[n].Cells[3].Value = item["ItemPrice"].ToString();
            }

        }

        private void buttonfetch_Click(object sender, EventArgs e)
        {
            
            foreach (DataGridViewRow item in dataGridViewToFetch.Rows)
            {
                if ((bool)item.Cells[0].Value == true)
                {
                    int n = dataGridViewFetched.Rows.Add();
                    dataGridViewFetched.Rows[n].Cells[0].Value = item.Cells[1].Value.ToString();
                    dataGridViewFetched.Rows[n].Cells[1].Value = item.Cells[2].Value.ToString();
                    dataGridViewFetched.Rows[n].Cells[2].Value = item.Cells[3].Value.ToString();
                }
            }
        }

        private void buttonConfirm_Click(object sender, EventArgs e)
        {
            dataGridViewFetched.Font.Bold.ToString();
            dataGridViewFetched.ForeColor = Color.Maroon;
            textBoxPrint.Clear();
            textBoxPrint.AppendText(" Hi-Wash Laundry Service " + Environment.NewLine);
            textBoxPrint.AppendText("\t" + "-----------------------------------------------------------------------------------------------------------------------" + Environment.NewLine);
            textBoxPrint.AppendText("Bill Number : "+textBoxBillNumber.Text + Environment.NewLine);
            textBoxPrint.AppendText("Booking DaTe : " + DateTime.Now.Date.ToString()  + Environment.NewLine);
            textBoxPrint.AppendText("Delivery Date : " + dateTimePickerDate.Text  + Environment.NewLine);
            textBoxPrint.AppendText("Customer Name : " + textBoxCustomerName.Text  + Environment.NewLine);
            textBoxPrint.AppendText("Customer Address : " + textBoxCustomerAddress.Text  + Environment.NewLine);
            textBoxPrint.AppendText("Customer Phone  :  " + textBoxCustomerPhone.Text  + Environment.NewLine);
            textBoxPrint.AppendText("\t" + "-----------------------------------------------------------------------------------------------------------------------" + Environment.NewLine);
            textBoxPrint.AppendText("  " + "Qty" + "\t" + "Name" + "\t" + "Price" + Environment.NewLine);

            for (int i = 0; i < dataGridViewFetched.Rows.Count; i++)
            {
                int totalprice = 0;
                totalprice = totalprice + Convert.ToInt32(dataGridViewFetched.Rows[i].Cells[2].Value);
            }
            for (int i = 0; i < dataGridViewFetched.Rows.Count; i++)
            {
                textBoxPrint.AppendText(" " + dataGridViewFetched.Rows[i].Cells[3].Value + "\t" + dataGridViewFetched.Rows[i].Cells[1].Value + "\t" + dataGridViewFetched.Rows[i].Cells[2].Value + Environment.NewLine);
            }

            for (int i = 0; i < dataGridViewFetched.Rows.Count; i++)
            {
                textBoxSelectedItem.AppendText(dataGridViewFetched.Rows[i].Cells[3].Value.ToString() + " * " + dataGridViewFetched.Rows[i].Cells[1].Value.ToString() + " - ");
            }
            float discount = Convert.ToInt32(textBoxDiscount.Text);
            discount = discount / 100;
            discount = discount * TotalPrice;
            TotalPrice = TotalPrice - Convert.ToInt32(discount);
            textBoxTotalPrice.Text = TotalPrice.ToString();
            textBoxPrint.AppendText("\t" + "-----------------------------------------------------------------------------------------------------------------------" + Environment.NewLine);
            textBoxPrint.AppendText(Convert.ToString("\t"+"Total = " + textBoxTotalPrice.Text + Environment.NewLine));
            textBoxPrint.AppendText("\t"+"Thank You For Visit" + Environment.NewLine);
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewCell Del in dataGridViewFetched.SelectedCells)
            {
                if (Del.Selected)
                {
                    dataGridViewFetched.Rows.RemoveAt(Del.RowIndex);
                    //textBox1.AppendText(Del.ToString());
                    string s = textBoxSelectedItem.Text;
                    if (s.Length > 1)
                    {
                        s = s.Substring(0, s.Length - 10);
                    }
                    else
                    {
                        s = "0";
                    }
                    textBoxSelectedItem.Text = s;
                    TotalPrice = 0;
                    int Qty;
                    int Price;


                    for (int i = 0; i < dataGridViewFetched.Rows.Count; ++i)
                    {
                        Qty = Convert.ToInt32(dataGridViewFetched.Rows[i].Cells[3].Value);
                        Price = Convert.ToInt32(dataGridViewFetched.Rows[i].Cells[2].Value);
                        TotalPrice = Qty * Price;

                    }

                    textBoxTotalPrice.Text = TotalPrice.ToString();
                    textBoxPrint.Clear();
                    textBoxPrint.AppendText("\t" + "--------------------------------------------------------------------------------------" + Environment.NewLine);
                    textBoxPrint.AppendText("  " + "Qty" + "\t" + "Name" + "\t\t" + "Price" + Environment.NewLine);
                    for (int i = 0; i < dataGridViewFetched.Rows.Count; i++)
                    {
                        textBoxPrint.AppendText(" " + dataGridViewFetched.Rows[i].Cells[3].Value + "\t" + dataGridViewFetched.Rows[i].Cells[1].Value + "\t" + dataGridViewFetched.Rows[i].Cells[2].Value + Environment.NewLine);
                    }
                }
            }
        }

        private void dataGridViewFetched_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            TotalPrice = 0;
            int Qty;
            int Price;
            for (int i = 0; i < dataGridViewFetched.Rows.Count; ++i)
            {
                Qty = Convert.ToInt32(dataGridViewFetched.Rows[i].Cells[3].Value);
                Price = Convert.ToInt32(dataGridViewFetched.Rows[i].Cells[2].Value);
                TotalPrice += Qty * Price;
            }
            textBoxTotalPrice.Text = TotalPrice.ToString();
        }

        private void buttonPrint_Click(object sender, EventArgs e)
        {
            if (ValidateChildren(ValidationConstraints.Enabled))
            {
                if (string.IsNullOrWhiteSpace(textBoxCustomerName.Text))
                {
                    Cancel = true;
                    textBoxCustomerName.Focus();
                    errorProviderCname.SetError(textBoxCustomerName, "Customer Name Is Empty");
                }
                else
                {
                    Cancel = false;
                }

                if (string.IsNullOrWhiteSpace(textBoxCustomerPhone.Text))
                {
                    Cancel = true;
                    textBoxCustomerPhone.Focus();
                    errorProviderCPhone.SetError(textBoxCustomerPhone, "Customer Phone Number Is Empty");
                }
                else
                {
                    Cancel = false;
                }

                if (string.IsNullOrWhiteSpace(textBoxSelectedItem.Text))
                {
                    Cancel = true;
                    textBoxSelectedItem.Focus();
                    errorProviderSItem.SetError(textBoxSelectedItem, "Selected Item Is Empty");
                }
                else
                {
                    Cancel = false;
                }
            }

            try
            {
                if (!(textBoxSelectedItem.Text == "" || textBoxCustomerName.Text == "" ||
                    textBoxCustomerPhone.Text == ""))
                {
                    SqlCommand cmds = new SqlCommand("sp_SaveSales", Con);
                    cmds.CommandType = CommandType.StoredProcedure;
                    cmds.Parameters.AddWithValue("@CustomerName", textBoxCustomerName.Text);
                    cmds.Parameters.AddWithValue("@CustomerPhone", textBoxCustomerPhone.Text);
                    cmds.Parameters.AddWithValue("@Discount", textBoxDiscount.Text);
                    cmds.Parameters.AddWithValue("@TotalAmount", textBoxTotalPrice.Text);
                    cmds.Parameters.AddWithValue("@Items", textBoxSelectedItem.Text);
                    cmds.Parameters.AddWithValue("@SaleDate", DateTime.Now.Date.ToShortDateString().ToString());
                    using (Con)
                    {
                        cmds.ExecuteNonQuery();
                    }
                    MessageBox.Show("Inserted Successfully", MessageBoxIcon.Information.ToString());
                    //labelTp.Text = Convert.ToString("Price = " + textBoxTotalPrice.Text);
                    printPreviewDialogView.Document = printDocumentPrint;
                    printPreviewDialogView.ShowDialog();
                    textBoxCustomerPhone.Clear();
                    textBoxSelectedItem.Clear();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void printDocumentPrint_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            Bitmap BitmapObj = new Bitmap(this.textBoxPrint.Width, this.textBoxPrint.Height);
            textBoxPrint.DrawToBitmap(BitmapObj, new Rectangle(0, 0, this.textBoxPrint.Width, this.textBoxPrint.Height));
            e.Graphics.DrawImage(BitmapObj, 250, 90);
            //e.Graphics.DrawString(labelHiWashHeader.Text, new Font("verdana", 20, FontStyle.Bold), Brushes.Maroon, new Point(270, 20));
            //e.Graphics.DrawString(labelReciept.Text, new Font("verdana", 18, FontStyle.Bold), Brushes.Maroon, new Point(360, 50));
            //e.Graphics.DrawString(labelReceiptFooter.Text, new Font("verdana", 18, FontStyle.Bold), Brushes.Maroon, new Point(200, 500));
            //e.Graphics.DrawString(labelTp.Text, new Font("verdana", 16, FontStyle.Bold), Brushes.Maroon, new Point(350, 450));
        }

        private void LoadBillNumber()
        {
            SqlCommand cmd = new SqlCommand("SELECT MAX(BillId) + 1 as BillId FROM Sales", Con);
            using (SqlDataReader sdr = cmd.ExecuteReader())
            {
                if (sdr.Read())
                {
                    textBoxBillNumber.Text = sdr["BillId"].ToString();
                }
                sdr.Close();
            }

        }
    }
}
