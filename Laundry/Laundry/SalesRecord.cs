﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Laundry
{
    public partial class SalesRecord : Form
    {

        bool cancel;
        SqlConnection Con;
        public SalesRecord()
        {
            InitializeComponent();
            string cs = "Data Source=DESKTOP-NM1RRE;Initial Catalog=Laundry;Integrated Security=True";
            Con = new SqlConnection(cs);
            Con.Open();
        }

        private void buttonCanel_Click(object sender, EventArgs e)
        {
            DialogResult Result = MessageBox.Show("Are You Sure?", "Exit", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
            if (Result == DialogResult.OK)
            {
                this.Close();
            }
            else
            {

            }
        }

        private void textBoxPhoneNumber_TextChanged(object sender, EventArgs e)
        {
            if(ValidateChildren(ValidationConstraints.Enabled))
            {
                if(string.IsNullOrWhiteSpace(textBoxPhoneNumber.Text))
                {
                    cancel = true;
                    textBoxPhoneNumber.Focus();
                    errorProviderPhoneNumber.SetError(textBoxPhoneNumber, "Phone Number Is Empty");
                }
                else
                {
                    cancel = false;
                }

                try
                {
                    if (!(textBoxPhoneNumber.Text == ""))
                    {
                        SqlCommand cmd = new SqlCommand("sp_GetPhoneHistory", Con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@CustomerPhone", textBoxPhoneNumber.Text);
                        DataTable dt = new DataTable();
                        SqlDataAdapter sda = new SqlDataAdapter(cmd);
                        sda.Fill(dt);
                        dataGridViewSaleRecord.DataSource = dt;
                    }
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void textBoxCustomerName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if(!(textBoxCustomerName.Text == "" ))
                {
                    SqlCommand cmd = new SqlCommand("sp_GetByNameHistory", Con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BillId", textBoxCustomerName.Text);
                    DataTable dt = new DataTable();
                    SqlDataAdapter sda = new SqlDataAdapter(cmd);
                    sda.Fill(dt);
                    dataGridViewSaleRecord.DataSource = dt;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            int TotalPrice = 0;
            int Price;
            dateTimePickerHistory.CustomFormat = "dd-MM-yyyy";
            try
            {
                SqlCommand cmd = new SqlCommand("sp_GetByDateHistory", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SaleDate", dateTimePickerHistory.Text.ToString());                
                DataTable dt = new DataTable();
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                dataGridViewSaleRecord.DataSource = dt;
                for (int i = 0; i < dataGridViewSaleRecord.Rows.Count; ++i)
                {
                    Price = Convert.ToInt32(dataGridViewSaleRecord.Rows[i].Cells[2].Value);
                    TotalPrice += Price;
                }
                textBoxTotalSale.Text = Convert.ToString(TotalPrice);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
