﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Laundry
{
    public partial class LoginSetting : Form
    {
        bool Cancel;
        SqlConnection Con;
        public LoginSetting()
        {
            InitializeComponent();
            string cs = "Data Source=DESKTOP-NM1RRE;Initial Catalog=Laundry;Integrated Security=True";
            Con = new SqlConnection(cs);
            Con.Open();
        }

        private void buttonUpdatedUserName_Click(object sender, EventArgs e)
        {
            if(ValidateChildren(ValidationConstraints.Enabled))
            {
                if(string.IsNullOrWhiteSpace(textBoxUserName.Text))
                {
                    Cancel = true;
                    textBoxUserName.Focus();
                    errorProviderUserName.SetError(textBoxUserName, "User Name is Empty");
                }
                else
                {
                    Cancel = false;
                }
            }

            try
            {
                if(!(textBoxUserName.Text == ""))
                {
                    SqlCommand cmd = new SqlCommand("sp_UpdateUserName", Con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@LoginUserName", textBoxUserName.Text);
                    using (Con)
                    {
                        
                        cmd.ExecuteNonQuery();
                    }
                    MessageBox.Show("Updated Successfull", MessageBoxIcon.Information.ToString());
                    textBoxUserName.Clear();
                }
            }
            catch(Exception  ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void buttonUpdatePassword_Click(object sender, EventArgs e)
        {
            if (ValidateChildren(ValidationConstraints.Enabled))
            {
                if (string.IsNullOrWhiteSpace(textBoxPassword.Text))
                {
                    Cancel = true;
                    textBoxPassword.Focus();
                    errorProviderPassword.SetError(textBoxPassword, "Password is Empty");
                }
                else
                {
                    Cancel = false;
                }
            }

            try
            {
                if (!(textBoxPassword.Text == ""))
                {
                    SqlCommand cmd = new SqlCommand("sp_UpdatePassword", Con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@LoginPassword", textBoxPassword.Text);
                    using (Con)
                        
                    {
                        cmd.ExecuteNonQuery();
                    }
                    MessageBox.Show("Updated Successfull", MessageBoxIcon.Information.ToString());
                    textBoxUserName.Clear();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult Result = MessageBox.Show("Are You Sure?", "Exit", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
            if (Result == DialogResult.OK)
            {
                this.Close();
            }
            else
            {

            }
        }
    }
}
