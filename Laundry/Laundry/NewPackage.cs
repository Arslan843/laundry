﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace Laundry
{
    public partial class NewPackage : Form
    {
        bool Cancel;
        SqlConnection Con;
        public NewPackage()
        {
            InitializeComponent();
            string cs = "Data Source=DESKTOP-NM1RRE;Initial Catalog=Laundry;Integrated Security=True";
            Con = new SqlConnection(cs);
            Con.Open();
        }

        private void buttonCanel_Click(object sender, EventArgs e)
        {
            DialogResult Result = MessageBox.Show("Are You Sure?", "Exit", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
            if (Result == DialogResult.OK)
            {
                this.Close();
            }
            else
            {

            }
        }
        private void textBoxPhoneNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            SqlCommand cmd = new SqlCommand("sp_GetCustomerData", Con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@RegularCustomerPhoneNumber", textBoxPhoneNumber.Text);
            DataTable dt = new DataTable();
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            sda.Fill(dt);
            dataGridViewCustomerRecord.DataSource = dt;
            if(dt.Rows.Count > 0)
            {
                textBoxCustomerId.Text = dt.Rows[0]["RegularCustomerId"].ToString();
            }
            textBoxCustomerId.Enabled = false;
        }

        private void buttonConfirm_Click(object sender, EventArgs e)
        {
            if(ValidateChildren(ValidationConstraints.Enabled))
            {
                if(string.IsNullOrWhiteSpace(textBoxCustomerId.Text))
                {
                    Cancel = true;
                    textBoxCustomerId.Focus();
                    errorProviderCustomerId.SetError(textBoxCustomerId, "Customer Id Empty");
                }
                else
                {
                    Cancel = false;
                }

                if (string.IsNullOrWhiteSpace(textBoxPricePerPiece.Text))
                {
                    Cancel = true;
                    textBoxPricePerPiece.Focus();
                    errorProviderPricePerPiece.SetError(textBoxPricePerPiece, "Price Per Piece Empty");
                }
                else
                {
                    Cancel = false;
                }
                try
                {
                    if(!(textBoxCustomerId.Text == "" || textBoxPricePerPiece.Text == ""))
                    {
                        SqlCommand cmd = new SqlCommand("sp_SavePackage", Con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@CustomerId", textBoxCustomerId.Text);
                        cmd.Parameters.AddWithValue("@PackagesPricePerPiece", textBoxPricePerPiece.Text);
                        using (Con)
                        {
                            cmd.ExecuteNonQuery();
                        }
                        MessageBox.Show("Package Confirmed",MessageBoxIcon.Information.ToString());
                        textBoxCustomerId.Clear();
                        textBoxPricePerPiece.Clear();
                    }
                    else
                    {

                    }
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

            }
        }
    }
}
