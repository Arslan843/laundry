﻿namespace Laundry
{
    partial class ItemsSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel2 = new System.Windows.Forms.Panel();
            this.buttonCanel = new System.Windows.Forms.Button();
            this.LabelItemSettings = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dataGridViewItemRecord = new System.Windows.Forms.DataGridView();
            this.panelDeleted = new System.Windows.Forms.Panel();
            this.labelDeleted = new System.Windows.Forms.Label();
            this.labelDeleteId = new System.Windows.Forms.Label();
            this.buttonDeleted = new System.Windows.Forms.Button();
            this.textBoxDeleteId = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.textBoxUId = new System.Windows.Forms.TextBox();
            this.labelUpdateId = new System.Windows.Forms.Label();
            this.labelUpdated = new System.Windows.Forms.Label();
            this.labelUpdatedName = new System.Windows.Forms.Label();
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.labelUpdatedPrice = new System.Windows.Forms.Label();
            this.textBoxUPrice = new System.Windows.Forms.TextBox();
            this.textBoxUName = new System.Windows.Forms.TextBox();
            this.panelInsert = new System.Windows.Forms.Panel();
            this.labelInserted = new System.Windows.Forms.Label();
            this.labelItemName = new System.Windows.Forms.Label();
            this.buttonInsert = new System.Windows.Forms.Button();
            this.labelItemPrice = new System.Windows.Forms.Label();
            this.textBoxItemPrice = new System.Windows.Forms.TextBox();
            this.textBoxItemName = new System.Windows.Forms.TextBox();
            this.errorProviderInsertItemName = new System.Windows.Forms.ErrorProvider(this.components);
            this.errorProviderInsertItemPrice = new System.Windows.Forms.ErrorProvider(this.components);
            this.errorProviderUpdateId = new System.Windows.Forms.ErrorProvider(this.components);
            this.errorProviderUpdateName = new System.Windows.Forms.ErrorProvider(this.components);
            this.errorProviderItemPrice = new System.Windows.Forms.ErrorProvider(this.components);
            this.errorProviderDeleteid = new System.Windows.Forms.ErrorProvider(this.components);
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewItemRecord)).BeginInit();
            this.panelDeleted.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panelInsert.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderInsertItemName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderInsertItemPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderUpdateId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderUpdateName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderItemPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderDeleteid)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.buttonCanel);
            this.panel2.Controls.Add(this.LabelItemSettings);
            this.panel2.Location = new System.Drawing.Point(12, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(583, 38);
            this.panel2.TabIndex = 23;
            // 
            // buttonCanel
            // 
            this.buttonCanel.BackColor = System.Drawing.Color.DarkRed;
            this.buttonCanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonCanel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonCanel.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12.25F);
            this.buttonCanel.ForeColor = System.Drawing.Color.White;
            this.buttonCanel.Location = new System.Drawing.Point(549, 3);
            this.buttonCanel.Name = "buttonCanel";
            this.buttonCanel.Size = new System.Drawing.Size(29, 29);
            this.buttonCanel.TabIndex = 7;
            this.buttonCanel.Text = "X";
            this.buttonCanel.UseVisualStyleBackColor = false;
            this.buttonCanel.Click += new System.EventHandler(this.buttonCanel_Click);
            // 
            // LabelItemSettings
            // 
            this.LabelItemSettings.AutoSize = true;
            this.LabelItemSettings.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelItemSettings.ForeColor = System.Drawing.Color.Navy;
            this.LabelItemSettings.Location = new System.Drawing.Point(5, 10);
            this.LabelItemSettings.Name = "LabelItemSettings";
            this.LabelItemSettings.Size = new System.Drawing.Size(142, 22);
            this.LabelItemSettings.TabIndex = 4;
            this.LabelItemSettings.Text = "Items Settings";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.dataGridViewItemRecord);
            this.panel1.Controls.Add(this.panelDeleted);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panelInsert);
            this.panel1.Location = new System.Drawing.Point(12, 56);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(583, 582);
            this.panel1.TabIndex = 22;
            // 
            // dataGridViewItemRecord
            // 
            this.dataGridViewItemRecord.AllowUserToAddRows = false;
            this.dataGridViewItemRecord.AllowUserToDeleteRows = false;
            this.dataGridViewItemRecord.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewItemRecord.Location = new System.Drawing.Point(226, 14);
            this.dataGridViewItemRecord.Name = "dataGridViewItemRecord";
            this.dataGridViewItemRecord.ReadOnly = true;
            this.dataGridViewItemRecord.Size = new System.Drawing.Size(343, 554);
            this.dataGridViewItemRecord.TabIndex = 24;
            this.dataGridViewItemRecord.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewItemRecord_CellContentClick);
            // 
            // panelDeleted
            // 
            this.panelDeleted.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelDeleted.Controls.Add(this.labelDeleted);
            this.panelDeleted.Controls.Add(this.labelDeleteId);
            this.panelDeleted.Controls.Add(this.buttonDeleted);
            this.panelDeleted.Controls.Add(this.textBoxDeleteId);
            this.panelDeleted.Location = new System.Drawing.Point(9, 434);
            this.panelDeleted.Name = "panelDeleted";
            this.panelDeleted.Size = new System.Drawing.Size(211, 134);
            this.panelDeleted.TabIndex = 17;
            // 
            // labelDeleted
            // 
            this.labelDeleted.AutoSize = true;
            this.labelDeleted.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F);
            this.labelDeleted.ForeColor = System.Drawing.Color.Navy;
            this.labelDeleted.Location = new System.Drawing.Point(6, 10);
            this.labelDeleted.Name = "labelDeleted";
            this.labelDeleted.Size = new System.Drawing.Size(142, 18);
            this.labelDeleted.TabIndex = 16;
            this.labelDeleted.Text = "Delete Item Here";
            // 
            // labelDeleteId
            // 
            this.labelDeleteId.AutoSize = true;
            this.labelDeleteId.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDeleteId.ForeColor = System.Drawing.Color.Navy;
            this.labelDeleteId.Location = new System.Drawing.Point(6, 37);
            this.labelDeleteId.Name = "labelDeleteId";
            this.labelDeleteId.Size = new System.Drawing.Size(51, 15);
            this.labelDeleteId.TabIndex = 8;
            this.labelDeleteId.Text = "Item Id";
            // 
            // buttonDeleted
            // 
            this.buttonDeleted.BackColor = System.Drawing.Color.Navy;
            this.buttonDeleted.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonDeleted.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonDeleted.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12.25F);
            this.buttonDeleted.ForeColor = System.Drawing.Color.White;
            this.buttonDeleted.Location = new System.Drawing.Point(88, 85);
            this.buttonDeleted.Name = "buttonDeleted";
            this.buttonDeleted.Size = new System.Drawing.Size(92, 33);
            this.buttonDeleted.TabIndex = 10;
            this.buttonDeleted.Text = "Delete";
            this.buttonDeleted.UseVisualStyleBackColor = false;
            this.buttonDeleted.Click += new System.EventHandler(this.buttonDeleted_Click);
            // 
            // textBoxDeleteId
            // 
            this.textBoxDeleteId.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.textBoxDeleteId.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxDeleteId.Location = new System.Drawing.Point(9, 55);
            this.textBoxDeleteId.Multiline = true;
            this.textBoxDeleteId.Name = "textBoxDeleteId";
            this.textBoxDeleteId.Size = new System.Drawing.Size(171, 24);
            this.textBoxDeleteId.TabIndex = 9;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.textBoxUId);
            this.panel3.Controls.Add(this.labelUpdateId);
            this.panel3.Controls.Add(this.labelUpdated);
            this.panel3.Controls.Add(this.labelUpdatedName);
            this.panel3.Controls.Add(this.buttonUpdate);
            this.panel3.Controls.Add(this.labelUpdatedPrice);
            this.panel3.Controls.Add(this.textBoxUPrice);
            this.panel3.Controls.Add(this.textBoxUName);
            this.panel3.Location = new System.Drawing.Point(9, 197);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(211, 231);
            this.panel3.TabIndex = 17;
            // 
            // textBoxUId
            // 
            this.textBoxUId.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.textBoxUId.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxUId.Location = new System.Drawing.Point(9, 64);
            this.textBoxUId.Multiline = true;
            this.textBoxUId.Name = "textBoxUId";
            this.textBoxUId.Size = new System.Drawing.Size(171, 24);
            this.textBoxUId.TabIndex = 18;
            // 
            // labelUpdateId
            // 
            this.labelUpdateId.AutoSize = true;
            this.labelUpdateId.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUpdateId.ForeColor = System.Drawing.Color.Navy;
            this.labelUpdateId.Location = new System.Drawing.Point(6, 46);
            this.labelUpdateId.Name = "labelUpdateId";
            this.labelUpdateId.Size = new System.Drawing.Size(51, 15);
            this.labelUpdateId.TabIndex = 17;
            this.labelUpdateId.Text = "Item Id";
            // 
            // labelUpdated
            // 
            this.labelUpdated.AutoSize = true;
            this.labelUpdated.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F);
            this.labelUpdated.ForeColor = System.Drawing.Color.Navy;
            this.labelUpdated.Location = new System.Drawing.Point(6, 10);
            this.labelUpdated.Name = "labelUpdated";
            this.labelUpdated.Size = new System.Drawing.Size(148, 18);
            this.labelUpdated.TabIndex = 16;
            this.labelUpdated.Text = "Update Item Here";
            // 
            // labelUpdatedName
            // 
            this.labelUpdatedName.AutoSize = true;
            this.labelUpdatedName.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUpdatedName.ForeColor = System.Drawing.Color.Navy;
            this.labelUpdatedName.Location = new System.Drawing.Point(6, 91);
            this.labelUpdatedName.Name = "labelUpdatedName";
            this.labelUpdatedName.Size = new System.Drawing.Size(77, 15);
            this.labelUpdatedName.TabIndex = 8;
            this.labelUpdatedName.Text = "Item Name";
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.BackColor = System.Drawing.Color.Navy;
            this.buttonUpdate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonUpdate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonUpdate.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12.25F);
            this.buttonUpdate.ForeColor = System.Drawing.Color.White;
            this.buttonUpdate.Location = new System.Drawing.Point(88, 184);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(92, 33);
            this.buttonUpdate.TabIndex = 10;
            this.buttonUpdate.Text = "Update";
            this.buttonUpdate.UseVisualStyleBackColor = false;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // labelUpdatedPrice
            // 
            this.labelUpdatedPrice.AutoSize = true;
            this.labelUpdatedPrice.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUpdatedPrice.ForeColor = System.Drawing.Color.Navy;
            this.labelUpdatedPrice.Location = new System.Drawing.Point(6, 136);
            this.labelUpdatedPrice.Name = "labelUpdatedPrice";
            this.labelUpdatedPrice.Size = new System.Drawing.Size(74, 15);
            this.labelUpdatedPrice.TabIndex = 13;
            this.labelUpdatedPrice.Text = "Item Price";
            // 
            // textBoxUPrice
            // 
            this.textBoxUPrice.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.textBoxUPrice.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxUPrice.Location = new System.Drawing.Point(9, 154);
            this.textBoxUPrice.Multiline = true;
            this.textBoxUPrice.Name = "textBoxUPrice";
            this.textBoxUPrice.Size = new System.Drawing.Size(171, 24);
            this.textBoxUPrice.TabIndex = 15;
            // 
            // textBoxUName
            // 
            this.textBoxUName.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.textBoxUName.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxUName.Location = new System.Drawing.Point(9, 109);
            this.textBoxUName.Multiline = true;
            this.textBoxUName.Name = "textBoxUName";
            this.textBoxUName.Size = new System.Drawing.Size(171, 24);
            this.textBoxUName.TabIndex = 9;
            // 
            // panelInsert
            // 
            this.panelInsert.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelInsert.Controls.Add(this.labelInserted);
            this.panelInsert.Controls.Add(this.labelItemName);
            this.panelInsert.Controls.Add(this.buttonInsert);
            this.panelInsert.Controls.Add(this.labelItemPrice);
            this.panelInsert.Controls.Add(this.textBoxItemPrice);
            this.panelInsert.Controls.Add(this.textBoxItemName);
            this.panelInsert.Location = new System.Drawing.Point(9, 14);
            this.panelInsert.Name = "panelInsert";
            this.panelInsert.Size = new System.Drawing.Size(211, 177);
            this.panelInsert.TabIndex = 16;
            // 
            // labelInserted
            // 
            this.labelInserted.AutoSize = true;
            this.labelInserted.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F);
            this.labelInserted.ForeColor = System.Drawing.Color.Navy;
            this.labelInserted.Location = new System.Drawing.Point(6, 10);
            this.labelInserted.Name = "labelInserted";
            this.labelInserted.Size = new System.Drawing.Size(137, 18);
            this.labelInserted.TabIndex = 16;
            this.labelInserted.Text = "Insert Item Here";
            // 
            // labelItemName
            // 
            this.labelItemName.AutoSize = true;
            this.labelItemName.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelItemName.ForeColor = System.Drawing.Color.Navy;
            this.labelItemName.Location = new System.Drawing.Point(6, 37);
            this.labelItemName.Name = "labelItemName";
            this.labelItemName.Size = new System.Drawing.Size(77, 15);
            this.labelItemName.TabIndex = 8;
            this.labelItemName.Text = "Item Name";
            // 
            // buttonInsert
            // 
            this.buttonInsert.BackColor = System.Drawing.Color.Navy;
            this.buttonInsert.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonInsert.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonInsert.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12.25F);
            this.buttonInsert.ForeColor = System.Drawing.Color.White;
            this.buttonInsert.Location = new System.Drawing.Point(88, 130);
            this.buttonInsert.Name = "buttonInsert";
            this.buttonInsert.Size = new System.Drawing.Size(92, 33);
            this.buttonInsert.TabIndex = 10;
            this.buttonInsert.Text = "Insert";
            this.buttonInsert.UseVisualStyleBackColor = false;
            this.buttonInsert.Click += new System.EventHandler(this.buttonInsert_Click);
            // 
            // labelItemPrice
            // 
            this.labelItemPrice.AutoSize = true;
            this.labelItemPrice.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelItemPrice.ForeColor = System.Drawing.Color.Navy;
            this.labelItemPrice.Location = new System.Drawing.Point(6, 82);
            this.labelItemPrice.Name = "labelItemPrice";
            this.labelItemPrice.Size = new System.Drawing.Size(74, 15);
            this.labelItemPrice.TabIndex = 13;
            this.labelItemPrice.Text = "Item Price";
            // 
            // textBoxItemPrice
            // 
            this.textBoxItemPrice.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.textBoxItemPrice.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxItemPrice.Location = new System.Drawing.Point(9, 100);
            this.textBoxItemPrice.Multiline = true;
            this.textBoxItemPrice.Name = "textBoxItemPrice";
            this.textBoxItemPrice.Size = new System.Drawing.Size(171, 24);
            this.textBoxItemPrice.TabIndex = 15;
            // 
            // textBoxItemName
            // 
            this.textBoxItemName.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.textBoxItemName.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxItemName.Location = new System.Drawing.Point(9, 55);
            this.textBoxItemName.Multiline = true;
            this.textBoxItemName.Name = "textBoxItemName";
            this.textBoxItemName.Size = new System.Drawing.Size(171, 24);
            this.textBoxItemName.TabIndex = 9;
            // 
            // errorProviderInsertItemName
            // 
            this.errorProviderInsertItemName.ContainerControl = this;
            // 
            // errorProviderInsertItemPrice
            // 
            this.errorProviderInsertItemPrice.ContainerControl = this;
            // 
            // errorProviderUpdateId
            // 
            this.errorProviderUpdateId.ContainerControl = this;
            // 
            // errorProviderUpdateName
            // 
            this.errorProviderUpdateName.ContainerControl = this;
            // 
            // errorProviderItemPrice
            // 
            this.errorProviderItemPrice.ContainerControl = this;
            // 
            // errorProviderDeleteid
            // 
            this.errorProviderDeleteid.ContainerControl = this;
            // 
            // ItemsSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(607, 650);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ItemsSettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ItemsSettings";
            this.Load += new System.EventHandler(this.ItemsSettings_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewItemRecord)).EndInit();
            this.panelDeleted.ResumeLayout(false);
            this.panelDeleted.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panelInsert.ResumeLayout(false);
            this.panelInsert.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderInsertItemName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderInsertItemPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderUpdateId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderUpdateName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderItemPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderDeleteid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button buttonCanel;
        private System.Windows.Forms.Label LabelItemSettings;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textBoxItemPrice;
        private System.Windows.Forms.Label labelItemPrice;
        private System.Windows.Forms.Button buttonInsert;
        private System.Windows.Forms.TextBox textBoxItemName;
        private System.Windows.Forms.Label labelItemName;
        private System.Windows.Forms.Panel panelInsert;
        private System.Windows.Forms.Label labelInserted;
        private System.Windows.Forms.Panel panelDeleted;
        private System.Windows.Forms.Label labelDeleted;
        private System.Windows.Forms.Label labelDeleteId;
        private System.Windows.Forms.Button buttonDeleted;
        private System.Windows.Forms.TextBox textBoxDeleteId;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label labelUpdated;
        private System.Windows.Forms.Label labelUpdatedName;
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.Label labelUpdatedPrice;
        private System.Windows.Forms.TextBox textBoxUPrice;
        private System.Windows.Forms.TextBox textBoxUName;
        private System.Windows.Forms.DataGridView dataGridViewItemRecord;
        private System.Windows.Forms.TextBox textBoxUId;
        private System.Windows.Forms.Label labelUpdateId;
        private System.Windows.Forms.ErrorProvider errorProviderInsertItemName;
        private System.Windows.Forms.ErrorProvider errorProviderInsertItemPrice;
        private System.Windows.Forms.ErrorProvider errorProviderUpdateId;
        private System.Windows.Forms.ErrorProvider errorProviderUpdateName;
        private System.Windows.Forms.ErrorProvider errorProviderItemPrice;
        private System.Windows.Forms.ErrorProvider errorProviderDeleteid;
    }
}