﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Laundry
{
    public partial class Loader : Form
    {
        public Loader()
        {
            InitializeComponent();
        }

        private void timerLoader_Tick(object sender, EventArgs e)
        {
            LoaderProgress2.Width += Convert.ToInt32(6);
            if (LoaderProgress2.Width >= 511)
            {
                timerLoader.Stop();
                Login L = new Login();
                L.Show();
                this.Hide();
            }
        }
    }
}
