﻿namespace Laundry
{
    partial class NewPackage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel2 = new System.Windows.Forms.Panel();
            this.buttonCanel = new System.Windows.Forms.Button();
            this.LabelNewPackage = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dataGridViewCustomerRecord = new System.Windows.Forms.DataGridView();
            this.labelPricePerPiece = new System.Windows.Forms.Label();
            this.textBoxPricePerPiece = new System.Windows.Forms.TextBox();
            this.textBoxCustomerId = new System.Windows.Forms.TextBox();
            this.labelCustomerId = new System.Windows.Forms.Label();
            this.buttonConfirm = new System.Windows.Forms.Button();
            this.textBoxPhoneNumber = new System.Windows.Forms.TextBox();
            this.labelPhoneNumber = new System.Windows.Forms.Label();
            this.errorProviderCustomerId = new System.Windows.Forms.ErrorProvider(this.components);
            this.errorProviderPricePerPiece = new System.Windows.Forms.ErrorProvider(this.components);
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCustomerRecord)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderCustomerId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderPricePerPiece)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.buttonCanel);
            this.panel2.Controls.Add(this.LabelNewPackage);
            this.panel2.Location = new System.Drawing.Point(12, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(568, 38);
            this.panel2.TabIndex = 23;
            // 
            // buttonCanel
            // 
            this.buttonCanel.BackColor = System.Drawing.Color.DarkRed;
            this.buttonCanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonCanel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonCanel.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12.25F);
            this.buttonCanel.ForeColor = System.Drawing.Color.White;
            this.buttonCanel.Location = new System.Drawing.Point(531, 3);
            this.buttonCanel.Name = "buttonCanel";
            this.buttonCanel.Size = new System.Drawing.Size(29, 29);
            this.buttonCanel.TabIndex = 7;
            this.buttonCanel.Text = "X";
            this.buttonCanel.UseVisualStyleBackColor = false;
            this.buttonCanel.Click += new System.EventHandler(this.buttonCanel_Click);
            // 
            // LabelNewPackage
            // 
            this.LabelNewPackage.AutoSize = true;
            this.LabelNewPackage.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelNewPackage.ForeColor = System.Drawing.Color.Navy;
            this.LabelNewPackage.Location = new System.Drawing.Point(5, 10);
            this.LabelNewPackage.Name = "LabelNewPackage";
            this.LabelNewPackage.Size = new System.Drawing.Size(135, 22);
            this.LabelNewPackage.TabIndex = 4;
            this.LabelNewPackage.Text = "New Package";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.dataGridViewCustomerRecord);
            this.panel1.Controls.Add(this.labelPricePerPiece);
            this.panel1.Controls.Add(this.textBoxPricePerPiece);
            this.panel1.Controls.Add(this.textBoxCustomerId);
            this.panel1.Controls.Add(this.labelCustomerId);
            this.panel1.Controls.Add(this.buttonConfirm);
            this.panel1.Controls.Add(this.textBoxPhoneNumber);
            this.panel1.Controls.Add(this.labelPhoneNumber);
            this.panel1.Location = new System.Drawing.Point(12, 56);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(568, 342);
            this.panel1.TabIndex = 22;
            // 
            // dataGridViewCustomerRecord
            // 
            this.dataGridViewCustomerRecord.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewCustomerRecord.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewCustomerRecord.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewCustomerRecord.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewCustomerRecord.Location = new System.Drawing.Point(53, 78);
            this.dataGridViewCustomerRecord.Name = "dataGridViewCustomerRecord";
            this.dataGridViewCustomerRecord.Size = new System.Drawing.Size(460, 124);
            this.dataGridViewCustomerRecord.TabIndex = 21;
            // 
            // labelPricePerPiece
            // 
            this.labelPricePerPiece.AutoSize = true;
            this.labelPricePerPiece.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPricePerPiece.ForeColor = System.Drawing.Color.Navy;
            this.labelPricePerPiece.Location = new System.Drawing.Point(50, 272);
            this.labelPricePerPiece.Name = "labelPricePerPiece";
            this.labelPricePerPiece.Size = new System.Drawing.Size(108, 15);
            this.labelPricePerPiece.TabIndex = 20;
            this.labelPricePerPiece.Text = "Price Per Piece";
            // 
            // textBoxPricePerPiece
            // 
            this.textBoxPricePerPiece.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.textBoxPricePerPiece.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPricePerPiece.Location = new System.Drawing.Point(53, 290);
            this.textBoxPricePerPiece.Multiline = true;
            this.textBoxPricePerPiece.Name = "textBoxPricePerPiece";
            this.textBoxPricePerPiece.Size = new System.Drawing.Size(171, 24);
            this.textBoxPricePerPiece.TabIndex = 17;
            // 
            // textBoxCustomerId
            // 
            this.textBoxCustomerId.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.textBoxCustomerId.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCustomerId.Location = new System.Drawing.Point(53, 234);
            this.textBoxCustomerId.Multiline = true;
            this.textBoxCustomerId.Name = "textBoxCustomerId";
            this.textBoxCustomerId.Size = new System.Drawing.Size(171, 24);
            this.textBoxCustomerId.TabIndex = 15;
            // 
            // labelCustomerId
            // 
            this.labelCustomerId.AutoSize = true;
            this.labelCustomerId.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCustomerId.ForeColor = System.Drawing.Color.Navy;
            this.labelCustomerId.Location = new System.Drawing.Point(50, 216);
            this.labelCustomerId.Name = "labelCustomerId";
            this.labelCustomerId.Size = new System.Drawing.Size(86, 15);
            this.labelCustomerId.TabIndex = 13;
            this.labelCustomerId.Text = "Customer Id";
            // 
            // buttonConfirm
            // 
            this.buttonConfirm.BackColor = System.Drawing.Color.Navy;
            this.buttonConfirm.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonConfirm.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonConfirm.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12.25F);
            this.buttonConfirm.ForeColor = System.Drawing.Color.White;
            this.buttonConfirm.Location = new System.Drawing.Point(425, 292);
            this.buttonConfirm.Name = "buttonConfirm";
            this.buttonConfirm.Size = new System.Drawing.Size(92, 33);
            this.buttonConfirm.TabIndex = 10;
            this.buttonConfirm.Text = "Confirm";
            this.buttonConfirm.UseVisualStyleBackColor = false;
            this.buttonConfirm.Click += new System.EventHandler(this.buttonConfirm_Click);
            // 
            // textBoxPhoneNumber
            // 
            this.textBoxPhoneNumber.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.textBoxPhoneNumber.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPhoneNumber.Location = new System.Drawing.Point(53, 48);
            this.textBoxPhoneNumber.Multiline = true;
            this.textBoxPhoneNumber.Name = "textBoxPhoneNumber";
            this.textBoxPhoneNumber.Size = new System.Drawing.Size(206, 24);
            this.textBoxPhoneNumber.TabIndex = 9;
            this.textBoxPhoneNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxPhoneNumber_KeyPress);
            // 
            // labelPhoneNumber
            // 
            this.labelPhoneNumber.AutoSize = true;
            this.labelPhoneNumber.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPhoneNumber.ForeColor = System.Drawing.Color.Navy;
            this.labelPhoneNumber.Location = new System.Drawing.Point(50, 30);
            this.labelPhoneNumber.Name = "labelPhoneNumber";
            this.labelPhoneNumber.Size = new System.Drawing.Size(209, 15);
            this.labelPhoneNumber.TabIndex = 8;
            this.labelPhoneNumber.Text = "Enter Customer Phone Number";
            // 
            // errorProviderCustomerId
            // 
            this.errorProviderCustomerId.ContainerControl = this;
            // 
            // errorProviderPricePerPiece
            // 
            this.errorProviderPricePerPiece.ContainerControl = this;
            // 
            // NewPackage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(613, 413);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "NewPackage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "NewPackage";
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCustomerRecord)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderCustomerId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderPricePerPiece)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button buttonCanel;
        private System.Windows.Forms.Label LabelNewPackage;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textBoxCustomerId;
        private System.Windows.Forms.Label labelCustomerId;
        private System.Windows.Forms.Button buttonConfirm;
        private System.Windows.Forms.TextBox textBoxPhoneNumber;
        private System.Windows.Forms.Label labelPhoneNumber;
        private System.Windows.Forms.DataGridView dataGridViewCustomerRecord;
        private System.Windows.Forms.Label labelPricePerPiece;
        private System.Windows.Forms.TextBox textBoxPricePerPiece;
        private System.Windows.Forms.ErrorProvider errorProviderCustomerId;
        private System.Windows.Forms.ErrorProvider errorProviderPricePerPiece;
    }
}