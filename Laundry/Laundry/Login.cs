﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace Laundry
{
    public partial class Login : Form
    {
        SqlConnection Con;
        public Login()
        {
            InitializeComponent();
            string cs = "Data Source=DESKTOP-NM1RRE;Initial Catalog=Laundry;Integrated Security=True";
            Con = new SqlConnection(cs);
            Con.Open();
        }

        private void buttonCanel_Click(object sender, EventArgs e)
        {
            DialogResult Result = MessageBox.Show("Are You Sure?","Exit" , MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
            if (Result == DialogResult.OK)
            {
                Application.Exit();
            }
            else
            {

            }
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            SqlCommand cmd = new SqlCommand("sp_Login",Con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@LoginUserName", textBoxUserName.Text);
            cmd.Parameters.AddWithValue("@LoginPassword", textBoxPassword.Text);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            sda.Fill(dt);
            if(dt.Rows.Count > 0)
            {
                this.Close();
                MainPanel Mp = new MainPanel();
                Mp.Show();
            }
            else
            {
                MessageBox.Show("Invalid UserName And Password",MessageBoxIcon.Exclamation.ToString());
            }
                
        }

        private void checkBoxShowPassword_CheckedChanged(object sender, EventArgs e)
        {
            if(checkBoxShowPassword.Checked)
            {
                textBoxPassword.UseSystemPasswordChar = true;
            }
            else
            {
                textBoxPassword.UseSystemPasswordChar = false;
            }
        }
    }
}
