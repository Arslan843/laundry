﻿namespace Laundry
{
    partial class MainPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panelMenu = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelDay = new System.Windows.Forms.Label();
            this.labelDate = new System.Windows.Forms.Label();
            this.labelSecond = new System.Windows.Forms.Label();
            this.buttonExit = new System.Windows.Forms.Button();
            this.labelTime = new System.Windows.Forms.Label();
            this.buttonRecord = new System.Windows.Forms.Button();
            this.buttonLogSettings = new System.Windows.Forms.Button();
            this.buttonSettings = new System.Windows.Forms.Button();
            this.buttonPackage = new System.Windows.Forms.Button();
            this.buttonNewCustomer = new System.Windows.Forms.Button();
            this.buttonorder = new System.Windows.Forms.Button();
            this.timerClock = new System.Windows.Forms.Timer(this.components);
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.Labelpowered = new System.Windows.Forms.Label();
            this.panelMenu.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelMenu
            // 
            this.panelMenu.BackColor = System.Drawing.Color.Navy;
            this.panelMenu.Controls.Add(this.panel1);
            this.panelMenu.Controls.Add(this.buttonRecord);
            this.panelMenu.Controls.Add(this.buttonLogSettings);
            this.panelMenu.Controls.Add(this.buttonSettings);
            this.panelMenu.Controls.Add(this.buttonPackage);
            this.panelMenu.Controls.Add(this.buttonNewCustomer);
            this.panelMenu.Controls.Add(this.buttonorder);
            this.panelMenu.Location = new System.Drawing.Point(-12, -5);
            this.panelMenu.Name = "panelMenu";
            this.panelMenu.Size = new System.Drawing.Size(317, 798);
            this.panelMenu.TabIndex = 8;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.YellowGreen;
            this.panel1.Controls.Add(this.labelDay);
            this.panel1.Controls.Add(this.labelDate);
            this.panel1.Controls.Add(this.labelSecond);
            this.panel1.Controls.Add(this.buttonExit);
            this.panel1.Controls.Add(this.labelTime);
            this.panel1.Location = new System.Drawing.Point(3, 581);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(321, 214);
            this.panel1.TabIndex = 21;
            // 
            // labelDay
            // 
            this.labelDay.AutoSize = true;
            this.labelDay.BackColor = System.Drawing.Color.Transparent;
            this.labelDay.Font = new System.Drawing.Font("Dubai", 14F);
            this.labelDay.ForeColor = System.Drawing.Color.Navy;
            this.labelDay.Location = new System.Drawing.Point(190, 110);
            this.labelDay.Name = "labelDay";
            this.labelDay.Size = new System.Drawing.Size(64, 32);
            this.labelDay.TabIndex = 25;
            this.labelDay.Text = "Friday";
            // 
            // labelDate
            // 
            this.labelDate.AutoSize = true;
            this.labelDate.BackColor = System.Drawing.Color.Transparent;
            this.labelDate.Font = new System.Drawing.Font("Dubai", 14F);
            this.labelDate.ForeColor = System.Drawing.Color.Navy;
            this.labelDate.Location = new System.Drawing.Point(61, 110);
            this.labelDate.Name = "labelDate";
            this.labelDate.Size = new System.Drawing.Size(121, 32);
            this.labelDate.TabIndex = 24;
            this.labelDate.Text = "Sept 21 2018";
            // 
            // labelSecond
            // 
            this.labelSecond.AutoSize = true;
            this.labelSecond.BackColor = System.Drawing.Color.Transparent;
            this.labelSecond.Font = new System.Drawing.Font("Dubai", 14F);
            this.labelSecond.ForeColor = System.Drawing.Color.Navy;
            this.labelSecond.Location = new System.Drawing.Point(235, 61);
            this.labelSecond.Name = "labelSecond";
            this.labelSecond.Size = new System.Drawing.Size(35, 32);
            this.labelSecond.TabIndex = 23;
            this.labelSecond.Text = "12";
            // 
            // buttonExit
            // 
            this.buttonExit.BackColor = System.Drawing.Color.Navy;
            this.buttonExit.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonExit.Font = new System.Drawing.Font("Arial Rounded MT Bold", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExit.ForeColor = System.Drawing.Color.White;
            this.buttonExit.Location = new System.Drawing.Point(30, 155);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(263, 31);
            this.buttonExit.TabIndex = 22;
            this.buttonExit.Text = "Exit";
            this.buttonExit.UseVisualStyleBackColor = false;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // labelTime
            // 
            this.labelTime.AutoSize = true;
            this.labelTime.BackColor = System.Drawing.Color.Transparent;
            this.labelTime.Font = new System.Drawing.Font("Dubai", 48F);
            this.labelTime.ForeColor = System.Drawing.Color.Navy;
            this.labelTime.Location = new System.Drawing.Point(61, 20);
            this.labelTime.Name = "labelTime";
            this.labelTime.Size = new System.Drawing.Size(197, 108);
            this.labelTime.TabIndex = 9;
            this.labelTime.Text = "22:22";
            // 
            // buttonRecord
            // 
            this.buttonRecord.BackColor = System.Drawing.Color.YellowGreen;
            this.buttonRecord.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonRecord.Font = new System.Drawing.Font("Arial Rounded MT Bold", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRecord.ForeColor = System.Drawing.Color.White;
            this.buttonRecord.Location = new System.Drawing.Point(33, 299);
            this.buttonRecord.Name = "buttonRecord";
            this.buttonRecord.Size = new System.Drawing.Size(263, 88);
            this.buttonRecord.TabIndex = 18;
            this.buttonRecord.Text = "Sales Record";
            this.buttonRecord.UseVisualStyleBackColor = false;
            this.buttonRecord.Click += new System.EventHandler(this.buttonRecord_Click);
            // 
            // buttonLogSettings
            // 
            this.buttonLogSettings.BackColor = System.Drawing.Color.YellowGreen;
            this.buttonLogSettings.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonLogSettings.Font = new System.Drawing.Font("Arial Rounded MT Bold", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonLogSettings.ForeColor = System.Drawing.Color.White;
            this.buttonLogSettings.Location = new System.Drawing.Point(33, 487);
            this.buttonLogSettings.Name = "buttonLogSettings";
            this.buttonLogSettings.Size = new System.Drawing.Size(263, 88);
            this.buttonLogSettings.TabIndex = 19;
            this.buttonLogSettings.Text = "Login Settings";
            this.buttonLogSettings.UseVisualStyleBackColor = false;
            this.buttonLogSettings.Click += new System.EventHandler(this.buttonLogSettings_Click);
            // 
            // buttonSettings
            // 
            this.buttonSettings.BackColor = System.Drawing.Color.YellowGreen;
            this.buttonSettings.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonSettings.Font = new System.Drawing.Font("Arial Rounded MT Bold", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSettings.ForeColor = System.Drawing.Color.White;
            this.buttonSettings.Location = new System.Drawing.Point(33, 393);
            this.buttonSettings.Name = "buttonSettings";
            this.buttonSettings.Size = new System.Drawing.Size(263, 88);
            this.buttonSettings.TabIndex = 17;
            this.buttonSettings.Text = "Items Settings";
            this.buttonSettings.UseVisualStyleBackColor = false;
            this.buttonSettings.Click += new System.EventHandler(this.buttonSettings_Click);
            // 
            // buttonPackage
            // 
            this.buttonPackage.BackColor = System.Drawing.Color.YellowGreen;
            this.buttonPackage.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonPackage.Font = new System.Drawing.Font("Arial Rounded MT Bold", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPackage.ForeColor = System.Drawing.Color.White;
            this.buttonPackage.Location = new System.Drawing.Point(33, 111);
            this.buttonPackage.Name = "buttonPackage";
            this.buttonPackage.Size = new System.Drawing.Size(263, 88);
            this.buttonPackage.TabIndex = 16;
            this.buttonPackage.Text = "New Package";
            this.buttonPackage.UseVisualStyleBackColor = false;
            this.buttonPackage.Click += new System.EventHandler(this.buttonPackage_Click);
            // 
            // buttonNewCustomer
            // 
            this.buttonNewCustomer.BackColor = System.Drawing.Color.YellowGreen;
            this.buttonNewCustomer.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonNewCustomer.Font = new System.Drawing.Font("Arial Rounded MT Bold", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNewCustomer.ForeColor = System.Drawing.Color.White;
            this.buttonNewCustomer.Location = new System.Drawing.Point(33, 17);
            this.buttonNewCustomer.Name = "buttonNewCustomer";
            this.buttonNewCustomer.Size = new System.Drawing.Size(263, 88);
            this.buttonNewCustomer.TabIndex = 15;
            this.buttonNewCustomer.Text = "New Customer";
            this.buttonNewCustomer.UseVisualStyleBackColor = false;
            this.buttonNewCustomer.Click += new System.EventHandler(this.buttonNewCustomer_Click);
            // 
            // buttonorder
            // 
            this.buttonorder.BackColor = System.Drawing.Color.YellowGreen;
            this.buttonorder.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonorder.Font = new System.Drawing.Font("Arial Rounded MT Bold", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonorder.ForeColor = System.Drawing.Color.White;
            this.buttonorder.Location = new System.Drawing.Point(33, 205);
            this.buttonorder.Name = "buttonorder";
            this.buttonorder.Size = new System.Drawing.Size(263, 88);
            this.buttonorder.TabIndex = 20;
            this.buttonorder.Text = "Place Order";
            this.buttonorder.UseVisualStyleBackColor = false;
            this.buttonorder.Click += new System.EventHandler(this.buttonorder_Click);
            // 
            // timerClock
            // 
            this.timerClock.Interval = 1000;
            this.timerClock.Tick += new System.EventHandler(this.timerClock_Tick);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.YellowGreen;
            this.panel2.Location = new System.Drawing.Point(1292, -5);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(79, 630);
            this.panel2.TabIndex = 9;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Navy;
            this.panel3.Location = new System.Drawing.Point(1241, -2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(64, 627);
            this.panel3.TabIndex = 10;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.YellowGreen;
            this.panel4.Controls.Add(this.panel5);
            this.panel4.Location = new System.Drawing.Point(1241, 631);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(130, 159);
            this.panel4.TabIndex = 27;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Navy;
            this.panel5.Location = new System.Drawing.Point(0, 77);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(130, 82);
            this.panel5.TabIndex = 28;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Navy;
            this.panel6.Controls.Add(this.label1);
            this.panel6.Controls.Add(this.Labelpowered);
            this.panel6.Location = new System.Drawing.Point(311, 731);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(924, 32);
            this.panel6.TabIndex = 29;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.YellowGreen;
            this.label1.Location = new System.Drawing.Point(670, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(251, 22);
            this.label1.TabIndex = 31;
            this.label1.Text = "Copyrights Reserved 2018";
            // 
            // Labelpowered
            // 
            this.Labelpowered.AutoSize = true;
            this.Labelpowered.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Labelpowered.ForeColor = System.Drawing.Color.YellowGreen;
            this.Labelpowered.Location = new System.Drawing.Point(7, 5);
            this.Labelpowered.Name = "Labelpowered";
            this.Labelpowered.Size = new System.Drawing.Size(270, 22);
            this.Labelpowered.TabIndex = 30;
            this.Labelpowered.Text = "Powered By : Coding Troops";
            // 
            // MainPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1368, 788);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panelMenu);
            this.Cursor = System.Windows.Forms.Cursors.Hand;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MainPanel";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "\\";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainPanel_Load);
            this.panelMenu.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panelMenu;
        private System.Windows.Forms.Button buttonNewCustomer;
        private System.Windows.Forms.Button buttonorder;
        private System.Windows.Forms.Button buttonLogSettings;
        private System.Windows.Forms.Button buttonRecord;
        private System.Windows.Forms.Button buttonSettings;
        private System.Windows.Forms.Button buttonPackage;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.Label labelTime;
        private System.Windows.Forms.Label labelSecond;
        private System.Windows.Forms.Label labelDate;
        private System.Windows.Forms.Label labelDay;
        private System.Windows.Forms.Timer timerClock;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label Labelpowered;
    }
}