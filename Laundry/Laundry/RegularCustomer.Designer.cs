﻿namespace Laundry
{
    partial class RegularCustomer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RegularCustomer));
            this.panel3 = new System.Windows.Forms.Panel();
            this.textBoxCustomerName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dateTimePickerDeliveryDate = new System.Windows.Forms.DateTimePicker();
            this.textBoxBillNumber = new System.Windows.Forms.TextBox();
            this.labelBillNumber = new System.Windows.Forms.Label();
            this.dateTimePickerDate = new System.Windows.Forms.DateTimePicker();
            this.labelDate = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelPricePerPiece = new System.Windows.Forms.Label();
            this.textBoxPricePerPiece = new System.Windows.Forms.TextBox();
            this.buttonfetch = new System.Windows.Forms.Button();
            this.labelSelectedItem = new System.Windows.Forms.Label();
            this.textBoxSelectedItem = new System.Windows.Forms.TextBox();
            this.textBoxCustomerPhone = new System.Windows.Forms.TextBox();
            this.labelCustomerPhone = new System.Windows.Forms.Label();
            this.textBoxCustomerId = new System.Windows.Forms.TextBox();
            this.labelCustomerId = new System.Windows.Forms.Label();
            this.dataGridViewToFetch = new System.Windows.Forms.DataGridView();
            this.selectItem = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ItemId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonConfirm = new System.Windows.Forms.Button();
            this.textBoxTotalPrice = new System.Windows.Forms.TextBox();
            this.labelTotalPrice = new System.Windows.Forms.Label();
            this.textBoxPrint = new System.Windows.Forms.TextBox();
            this.buttonPrint = new System.Windows.Forms.Button();
            this.buttonRemove = new System.Windows.Forms.Button();
            this.dataGridViewFetched = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.buttonCanel = new System.Windows.Forms.Button();
            this.LabelTypeOfCustomer = new System.Windows.Forms.Label();
            this.errorProviderCId = new System.Windows.Forms.ErrorProvider(this.components);
            this.errorProviderCPhone = new System.Windows.Forms.ErrorProvider(this.components);
            this.errorProviderSelectedItem = new System.Windows.Forms.ErrorProvider(this.components);
            this.errorProviderPPPiece = new System.Windows.Forms.ErrorProvider(this.components);
            this.printDocumentPrint = new System.Drawing.Printing.PrintDocument();
            this.printPreviewDialogView = new System.Windows.Forms.PrintPreviewDialog();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewToFetch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFetched)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderCId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderCPhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderSelectedItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderPPPiece)).BeginInit();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.textBoxCustomerName);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.dateTimePickerDeliveryDate);
            this.panel3.Controls.Add(this.textBoxBillNumber);
            this.panel3.Controls.Add(this.labelBillNumber);
            this.panel3.Controls.Add(this.dateTimePickerDate);
            this.panel3.Controls.Add(this.labelDate);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.labelPricePerPiece);
            this.panel3.Controls.Add(this.textBoxPricePerPiece);
            this.panel3.Controls.Add(this.buttonfetch);
            this.panel3.Controls.Add(this.labelSelectedItem);
            this.panel3.Controls.Add(this.textBoxSelectedItem);
            this.panel3.Controls.Add(this.textBoxCustomerPhone);
            this.panel3.Controls.Add(this.labelCustomerPhone);
            this.panel3.Controls.Add(this.textBoxCustomerId);
            this.panel3.Controls.Add(this.labelCustomerId);
            this.panel3.Controls.Add(this.dataGridViewToFetch);
            this.panel3.Controls.Add(this.buttonConfirm);
            this.panel3.Controls.Add(this.textBoxTotalPrice);
            this.panel3.Controls.Add(this.labelTotalPrice);
            this.panel3.Controls.Add(this.textBoxPrint);
            this.panel3.Controls.Add(this.buttonPrint);
            this.panel3.Controls.Add(this.buttonRemove);
            this.panel3.Controls.Add(this.dataGridViewFetched);
            this.panel3.Location = new System.Drawing.Point(12, 56);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(712, 636);
            this.panel3.TabIndex = 24;
            // 
            // textBoxCustomerName
            // 
            this.textBoxCustomerName.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.textBoxCustomerName.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCustomerName.Location = new System.Drawing.Point(359, 82);
            this.textBoxCustomerName.Multiline = true;
            this.textBoxCustomerName.Name = "textBoxCustomerName";
            this.textBoxCustomerName.Size = new System.Drawing.Size(200, 24);
            this.textBoxCustomerName.TabIndex = 69;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(356, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 15);
            this.label2.TabIndex = 68;
            this.label2.Text = "Customer Name";
            // 
            // dateTimePickerDeliveryDate
            // 
            this.dateTimePickerDeliveryDate.Location = new System.Drawing.Point(359, 246);
            this.dateTimePickerDeliveryDate.Name = "dateTimePickerDeliveryDate";
            this.dateTimePickerDeliveryDate.Size = new System.Drawing.Size(200, 20);
            this.dateTimePickerDeliveryDate.TabIndex = 67;
            // 
            // textBoxBillNumber
            // 
            this.textBoxBillNumber.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.textBoxBillNumber.Font = new System.Drawing.Font("Times New Roman", 14F);
            this.textBoxBillNumber.Location = new System.Drawing.Point(571, 64);
            this.textBoxBillNumber.Multiline = true;
            this.textBoxBillNumber.Name = "textBoxBillNumber";
            this.textBoxBillNumber.Size = new System.Drawing.Size(80, 34);
            this.textBoxBillNumber.TabIndex = 66;
            // 
            // labelBillNumber
            // 
            this.labelBillNumber.AutoSize = true;
            this.labelBillNumber.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBillNumber.ForeColor = System.Drawing.Color.Navy;
            this.labelBillNumber.Location = new System.Drawing.Point(568, 46);
            this.labelBillNumber.Name = "labelBillNumber";
            this.labelBillNumber.Size = new System.Drawing.Size(83, 15);
            this.labelBillNumber.TabIndex = 65;
            this.labelBillNumber.Text = "Bill Number";
            // 
            // dateTimePickerDate
            // 
            this.dateTimePickerDate.Location = new System.Drawing.Point(427, 272);
            this.dateTimePickerDate.Name = "dateTimePickerDate";
            this.dateTimePickerDate.Size = new System.Drawing.Size(200, 20);
            this.dateTimePickerDate.TabIndex = 64;
            this.dateTimePickerDate.Visible = false;
            // 
            // labelDate
            // 
            this.labelDate.AutoSize = true;
            this.labelDate.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDate.ForeColor = System.Drawing.Color.Navy;
            this.labelDate.Location = new System.Drawing.Point(356, 228);
            this.labelDate.Name = "labelDate";
            this.labelDate.Size = new System.Drawing.Size(140, 15);
            this.labelDate.TabIndex = 63;
            this.labelDate.Text = "Select Delivery Date";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(356, 602);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 15);
            this.label1.TabIndex = 62;
            this.label1.Text = "Total Price";
            // 
            // labelPricePerPiece
            // 
            this.labelPricePerPiece.AutoSize = true;
            this.labelPricePerPiece.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPricePerPiece.ForeColor = System.Drawing.Color.Navy;
            this.labelPricePerPiece.Location = new System.Drawing.Point(356, 175);
            this.labelPricePerPiece.Name = "labelPricePerPiece";
            this.labelPricePerPiece.Size = new System.Drawing.Size(108, 15);
            this.labelPricePerPiece.TabIndex = 61;
            this.labelPricePerPiece.Text = "Price Per Piece";
            // 
            // textBoxPricePerPiece
            // 
            this.textBoxPricePerPiece.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.textBoxPricePerPiece.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPricePerPiece.Location = new System.Drawing.Point(359, 193);
            this.textBoxPricePerPiece.Multiline = true;
            this.textBoxPricePerPiece.Name = "textBoxPricePerPiece";
            this.textBoxPricePerPiece.Size = new System.Drawing.Size(200, 24);
            this.textBoxPricePerPiece.TabIndex = 60;
            // 
            // buttonfetch
            // 
            this.buttonfetch.BackColor = System.Drawing.Color.Navy;
            this.buttonfetch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonfetch.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonfetch.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12.25F);
            this.buttonfetch.ForeColor = System.Drawing.Color.White;
            this.buttonfetch.Location = new System.Drawing.Point(9, 312);
            this.buttonfetch.Name = "buttonfetch";
            this.buttonfetch.Size = new System.Drawing.Size(75, 33);
            this.buttonfetch.TabIndex = 56;
            this.buttonfetch.Text = "Fetch";
            this.buttonfetch.UseVisualStyleBackColor = false;
            this.buttonfetch.Click += new System.EventHandler(this.buttonfetch_Click_1);
            // 
            // labelSelectedItem
            // 
            this.labelSelectedItem.AutoSize = true;
            this.labelSelectedItem.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSelectedItem.ForeColor = System.Drawing.Color.Navy;
            this.labelSelectedItem.Location = new System.Drawing.Point(356, 118);
            this.labelSelectedItem.Name = "labelSelectedItem";
            this.labelSelectedItem.Size = new System.Drawing.Size(97, 15);
            this.labelSelectedItem.TabIndex = 55;
            this.labelSelectedItem.Text = "Selected Item";
            // 
            // textBoxSelectedItem
            // 
            this.textBoxSelectedItem.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.textBoxSelectedItem.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxSelectedItem.Location = new System.Drawing.Point(359, 136);
            this.textBoxSelectedItem.Multiline = true;
            this.textBoxSelectedItem.Name = "textBoxSelectedItem";
            this.textBoxSelectedItem.Size = new System.Drawing.Size(200, 24);
            this.textBoxSelectedItem.TabIndex = 54;
            // 
            // textBoxCustomerPhone
            // 
            this.textBoxCustomerPhone.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.textBoxCustomerPhone.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCustomerPhone.Location = new System.Drawing.Point(359, 29);
            this.textBoxCustomerPhone.Multiline = true;
            this.textBoxCustomerPhone.Name = "textBoxCustomerPhone";
            this.textBoxCustomerPhone.Size = new System.Drawing.Size(200, 24);
            this.textBoxCustomerPhone.TabIndex = 52;
            // 
            // labelCustomerPhone
            // 
            this.labelCustomerPhone.AutoSize = true;
            this.labelCustomerPhone.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCustomerPhone.ForeColor = System.Drawing.Color.Navy;
            this.labelCustomerPhone.Location = new System.Drawing.Point(356, 11);
            this.labelCustomerPhone.Name = "labelCustomerPhone";
            this.labelCustomerPhone.Size = new System.Drawing.Size(115, 15);
            this.labelCustomerPhone.TabIndex = 51;
            this.labelCustomerPhone.Text = "Customer Phone";
            // 
            // textBoxCustomerId
            // 
            this.textBoxCustomerId.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.textBoxCustomerId.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCustomerId.Location = new System.Drawing.Point(9, 29);
            this.textBoxCustomerId.Multiline = true;
            this.textBoxCustomerId.Name = "textBoxCustomerId";
            this.textBoxCustomerId.Size = new System.Drawing.Size(200, 24);
            this.textBoxCustomerId.TabIndex = 50;
            this.textBoxCustomerId.TextChanged += new System.EventHandler(this.textBoxCustomerId_TextChanged);
            // 
            // labelCustomerId
            // 
            this.labelCustomerId.AutoSize = true;
            this.labelCustomerId.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCustomerId.ForeColor = System.Drawing.Color.Navy;
            this.labelCustomerId.Location = new System.Drawing.Point(6, 11);
            this.labelCustomerId.Name = "labelCustomerId";
            this.labelCustomerId.Size = new System.Drawing.Size(112, 15);
            this.labelCustomerId.TabIndex = 49;
            this.labelCustomerId.Text = "Get Customer Id";
            // 
            // dataGridViewToFetch
            // 
            this.dataGridViewToFetch.AllowUserToAddRows = false;
            this.dataGridViewToFetch.AllowUserToDeleteRows = false;
            this.dataGridViewToFetch.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewToFetch.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.selectItem,
            this.ItemId,
            this.ItemName});
            this.dataGridViewToFetch.Location = new System.Drawing.Point(9, 64);
            this.dataGridViewToFetch.Name = "dataGridViewToFetch";
            this.dataGridViewToFetch.Size = new System.Drawing.Size(341, 242);
            this.dataGridViewToFetch.TabIndex = 48;
            // 
            // selectItem
            // 
            this.selectItem.HeaderText = "Check";
            this.selectItem.Name = "selectItem";
            // 
            // ItemId
            // 
            this.ItemId.HeaderText = "ItemId";
            this.ItemId.Name = "ItemId";
            this.ItemId.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ItemId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ItemName
            // 
            this.ItemName.HeaderText = "ItemName";
            this.ItemName.Name = "ItemName";
            // 
            // buttonConfirm
            // 
            this.buttonConfirm.BackColor = System.Drawing.Color.Navy;
            this.buttonConfirm.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonConfirm.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonConfirm.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12.25F);
            this.buttonConfirm.ForeColor = System.Drawing.Color.White;
            this.buttonConfirm.Location = new System.Drawing.Point(9, 599);
            this.buttonConfirm.Name = "buttonConfirm";
            this.buttonConfirm.Size = new System.Drawing.Size(85, 33);
            this.buttonConfirm.TabIndex = 47;
            this.buttonConfirm.Text = "Confirm";
            this.buttonConfirm.UseVisualStyleBackColor = false;
            this.buttonConfirm.Click += new System.EventHandler(this.buttonConfirm_Click);
            // 
            // textBoxTotalPrice
            // 
            this.textBoxTotalPrice.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.textBoxTotalPrice.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxTotalPrice.Location = new System.Drawing.Point(440, 599);
            this.textBoxTotalPrice.Multiline = true;
            this.textBoxTotalPrice.Name = "textBoxTotalPrice";
            this.textBoxTotalPrice.Size = new System.Drawing.Size(100, 22);
            this.textBoxTotalPrice.TabIndex = 46;
            // 
            // labelTotalPrice
            // 
            this.labelTotalPrice.AutoSize = true;
            this.labelTotalPrice.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalPrice.ForeColor = System.Drawing.Color.Navy;
            this.labelTotalPrice.Location = new System.Drawing.Point(504, 716);
            this.labelTotalPrice.Name = "labelTotalPrice";
            this.labelTotalPrice.Size = new System.Drawing.Size(78, 15);
            this.labelTotalPrice.TabIndex = 45;
            this.labelTotalPrice.Text = "Total Price";
            // 
            // textBoxPrint
            // 
            this.textBoxPrint.Font = new System.Drawing.Font("Copperplate Gothic Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPrint.Location = new System.Drawing.Point(359, 272);
            this.textBoxPrint.Multiline = true;
            this.textBoxPrint.Name = "textBoxPrint";
            this.textBoxPrint.Size = new System.Drawing.Size(342, 321);
            this.textBoxPrint.TabIndex = 44;
            // 
            // buttonPrint
            // 
            this.buttonPrint.BackColor = System.Drawing.Color.MidnightBlue;
            this.buttonPrint.Font = new System.Drawing.Font("Copperplate Gothic Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPrint.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.buttonPrint.Location = new System.Drawing.Point(627, 608);
            this.buttonPrint.Name = "buttonPrint";
            this.buttonPrint.Size = new System.Drawing.Size(74, 24);
            this.buttonPrint.TabIndex = 43;
            this.buttonPrint.Text = "Print";
            this.buttonPrint.UseVisualStyleBackColor = false;
            this.buttonPrint.Click += new System.EventHandler(this.buttonPrint_Click);
            // 
            // buttonRemove
            // 
            this.buttonRemove.BackColor = System.Drawing.Color.Navy;
            this.buttonRemove.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonRemove.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonRemove.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12.25F);
            this.buttonRemove.ForeColor = System.Drawing.Color.White;
            this.buttonRemove.Location = new System.Drawing.Point(100, 599);
            this.buttonRemove.Name = "buttonRemove";
            this.buttonRemove.Size = new System.Drawing.Size(85, 33);
            this.buttonRemove.TabIndex = 39;
            this.buttonRemove.Text = "Remove";
            this.buttonRemove.UseVisualStyleBackColor = false;
            this.buttonRemove.Click += new System.EventHandler(this.buttonRemove_Click);
            // 
            // dataGridViewFetched
            // 
            this.dataGridViewFetched.AllowUserToAddRows = false;
            this.dataGridViewFetched.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewFetched.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.Name,
            this.Quantity});
            this.dataGridViewFetched.Location = new System.Drawing.Point(9, 351);
            this.dataGridViewFetched.Name = "dataGridViewFetched";
            this.dataGridViewFetched.Size = new System.Drawing.Size(341, 242);
            this.dataGridViewFetched.TabIndex = 38;
            this.dataGridViewFetched.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewFetched_CellValueChanged);
            // 
            // Id
            // 
            this.Id.HeaderText = "ItemId";
            this.Id.Name = "Id";
            // 
            // Name
            // 
            this.Name.HeaderText = "ItemName";
            this.Name.Name = "Name";
            // 
            // Quantity
            // 
            this.Quantity.HeaderText = "ItemQuantity";
            this.Quantity.Name = "Quantity";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.buttonCanel);
            this.panel2.Controls.Add(this.LabelTypeOfCustomer);
            this.panel2.Location = new System.Drawing.Point(12, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(712, 38);
            this.panel2.TabIndex = 25;
            // 
            // buttonCanel
            // 
            this.buttonCanel.BackColor = System.Drawing.Color.DarkRed;
            this.buttonCanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonCanel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonCanel.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12.25F);
            this.buttonCanel.ForeColor = System.Drawing.Color.White;
            this.buttonCanel.Location = new System.Drawing.Point(672, 3);
            this.buttonCanel.Name = "buttonCanel";
            this.buttonCanel.Size = new System.Drawing.Size(29, 29);
            this.buttonCanel.TabIndex = 7;
            this.buttonCanel.Text = "X";
            this.buttonCanel.UseVisualStyleBackColor = false;
            this.buttonCanel.Click += new System.EventHandler(this.buttonCanel_Click);
            // 
            // LabelTypeOfCustomer
            // 
            this.LabelTypeOfCustomer.AutoSize = true;
            this.LabelTypeOfCustomer.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelTypeOfCustomer.ForeColor = System.Drawing.Color.Navy;
            this.LabelTypeOfCustomer.Location = new System.Drawing.Point(5, 10);
            this.LabelTypeOfCustomer.Name = "LabelTypeOfCustomer";
            this.LabelTypeOfCustomer.Size = new System.Drawing.Size(211, 22);
            this.LabelTypeOfCustomer.TabIndex = 4;
            this.LabelTypeOfCustomer.Text = "For Regular Customer";
            // 
            // errorProviderCId
            // 
            this.errorProviderCId.ContainerControl = this;
            // 
            // errorProviderCPhone
            // 
            this.errorProviderCPhone.ContainerControl = this;
            // 
            // errorProviderSelectedItem
            // 
            this.errorProviderSelectedItem.ContainerControl = this;
            // 
            // errorProviderPPPiece
            // 
            this.errorProviderPPPiece.ContainerControl = this;
            // 
            // printDocumentPrint
            // 
            this.printDocumentPrint.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocumentPrint_PrintPage);
            // 
            // printPreviewDialogView
            // 
            this.printPreviewDialogView.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialogView.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialogView.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialogView.Enabled = true;
            this.printPreviewDialogView.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialogView.Icon")));
            this.printPreviewDialogView.Name = "printPreviewDialogView";
            this.printPreviewDialogView.Visible = false;
            // 
            // RegularCustomer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(733, 699);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewToFetch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFetched)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderCId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderCPhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderSelectedItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderPPPiece)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button buttonCanel;
        private System.Windows.Forms.Label LabelTypeOfCustomer;
        private System.Windows.Forms.TextBox textBoxTotalPrice;
        private System.Windows.Forms.Label labelTotalPrice;
        private System.Windows.Forms.TextBox textBoxPrint;
        private System.Windows.Forms.Button buttonPrint;
        private System.Windows.Forms.Button buttonRemove;
        private System.Windows.Forms.DataGridView dataGridViewFetched;
        private System.Windows.Forms.Button buttonConfirm;
        private System.Windows.Forms.Label labelSelectedItem;
        private System.Windows.Forms.TextBox textBoxSelectedItem;
        private System.Windows.Forms.TextBox textBoxCustomerPhone;
        private System.Windows.Forms.Label labelCustomerPhone;
        private System.Windows.Forms.TextBox textBoxCustomerId;
        private System.Windows.Forms.Label labelCustomerId;
        private System.Windows.Forms.DataGridView dataGridViewToFetch;
        private System.Windows.Forms.Button buttonfetch;
        private System.Windows.Forms.DataGridViewCheckBoxColumn selectItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemName;
        private System.Windows.Forms.Label labelPricePerPiece;
        private System.Windows.Forms.TextBox textBoxPricePerPiece;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Quantity;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ErrorProvider errorProviderCId;
        private System.Windows.Forms.ErrorProvider errorProviderCPhone;
        private System.Windows.Forms.ErrorProvider errorProviderSelectedItem;
        private System.Windows.Forms.ErrorProvider errorProviderPPPiece;
        private System.Drawing.Printing.PrintDocument printDocumentPrint;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialogView;
        private System.Windows.Forms.Label labelDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerDate;
        private System.Windows.Forms.TextBox textBoxBillNumber;
        private System.Windows.Forms.Label labelBillNumber;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dateTimePickerDeliveryDate;
        private System.Windows.Forms.TextBox textBoxCustomerName;
    }
}