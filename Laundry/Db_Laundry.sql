USE [Laundry]
GO
/****** Object:  Table [dbo].[Item]    Script Date: 1/11/2019 5:04:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Item](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[ItemName] [varchar](30) NOT NULL,
	[ItemPrice] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Login]    Script Date: 1/11/2019 5:04:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Login](
	[LoginUserName] [varchar](50) NOT NULL,
	[LoginPassword] [varchar](15) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Packages]    Script Date: 1/11/2019 5:04:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Packages](
	[PackageId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[PackagesPricePerPiece] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PackageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RegularCustomer]    Script Date: 1/11/2019 5:04:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RegularCustomer](
	[RegularCustomerId] [int] IDENTITY(1,1) NOT NULL,
	[RegularCustomerCnic] [varchar](20) NOT NULL,
	[RegularCustomerFirstName] [varchar](25) NOT NULL,
	[RegularCustomerLastName] [varchar](25) NOT NULL,
	[RegularCustomerPhoneNumber] [varchar](13) NOT NULL,
	[RegularCustomerCity] [varchar](20) NOT NULL,
	[RegularCustomerAddress] [varchar](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[RegularCustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sales]    Script Date: 1/11/2019 5:04:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sales](
	[BillId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerPhone] [varchar](13) NOT NULL,
	[TotalAmount] [int] NOT NULL,
	[CustomerName] [varchar](max) NOT NULL,
	[Discount] [int] NOT NULL,
	[Items] [varchar](max) NOT NULL,
	[SaleDate] [varchar](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[BillId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Item] ON 

INSERT [dbo].[Item] ([ItemId], [ItemName], [ItemPrice]) VALUES (1, N'BedSheet', 150)
INSERT [dbo].[Item] ([ItemId], [ItemName], [ItemPrice]) VALUES (3, N'Jacket', 200)
INSERT [dbo].[Item] ([ItemId], [ItemName], [ItemPrice]) VALUES (4, N'cotton suit', 150)
INSERT [dbo].[Item] ([ItemId], [ItemName], [ItemPrice]) VALUES (5, N'Jean', 77)
INSERT [dbo].[Item] ([ItemId], [ItemName], [ItemPrice]) VALUES (8, N'White Shirt', 30)
INSERT [dbo].[Item] ([ItemId], [ItemName], [ItemPrice]) VALUES (9, N'Cotton Jean', 17)
INSERT [dbo].[Item] ([ItemId], [ItemName], [ItemPrice]) VALUES (10, N'Round Neck Shirt', 75)
INSERT [dbo].[Item] ([ItemId], [ItemName], [ItemPrice]) VALUES (19, N'Touser', 11)
INSERT [dbo].[Item] ([ItemId], [ItemName], [ItemPrice]) VALUES (20, N'Nicki', 11)
INSERT [dbo].[Item] ([ItemId], [ItemName], [ItemPrice]) VALUES (21, N'Cotton Pant', 100)
SET IDENTITY_INSERT [dbo].[Item] OFF
INSERT [dbo].[Login] ([LoginUserName], [LoginPassword]) VALUES (N'admin', N'admin')
SET IDENTITY_INSERT [dbo].[Packages] ON 

INSERT [dbo].[Packages] ([PackageId], [CustomerId], [PackagesPricePerPiece]) VALUES (1, 1, 25)
INSERT [dbo].[Packages] ([PackageId], [CustomerId], [PackagesPricePerPiece]) VALUES (2, 4, 50)
INSERT [dbo].[Packages] ([PackageId], [CustomerId], [PackagesPricePerPiece]) VALUES (3, 5, 100)
INSERT [dbo].[Packages] ([PackageId], [CustomerId], [PackagesPricePerPiece]) VALUES (4, 6, 12)
INSERT [dbo].[Packages] ([PackageId], [CustomerId], [PackagesPricePerPiece]) VALUES (5, 7, 45)
INSERT [dbo].[Packages] ([PackageId], [CustomerId], [PackagesPricePerPiece]) VALUES (6, 7, 10)
INSERT [dbo].[Packages] ([PackageId], [CustomerId], [PackagesPricePerPiece]) VALUES (7, 8, 12)
INSERT [dbo].[Packages] ([PackageId], [CustomerId], [PackagesPricePerPiece]) VALUES (8, 9, 10)
INSERT [dbo].[Packages] ([PackageId], [CustomerId], [PackagesPricePerPiece]) VALUES (9, 10, 13)
INSERT [dbo].[Packages] ([PackageId], [CustomerId], [PackagesPricePerPiece]) VALUES (10, 11, 10)
INSERT [dbo].[Packages] ([PackageId], [CustomerId], [PackagesPricePerPiece]) VALUES (11, 12, 13)
INSERT [dbo].[Packages] ([PackageId], [CustomerId], [PackagesPricePerPiece]) VALUES (1009, 2010, 100)
SET IDENTITY_INSERT [dbo].[Packages] OFF
SET IDENTITY_INSERT [dbo].[RegularCustomer] ON 

INSERT [dbo].[RegularCustomer] ([RegularCustomerId], [RegularCustomerCnic], [RegularCustomerFirstName], [RegularCustomerLastName], [RegularCustomerPhoneNumber], [RegularCustomerCity], [RegularCustomerAddress]) VALUES (1, N'9030301141403', N'Muhammad', N'Adeel', N'03029752207', N'Gujranwala', N'Arsal Colony ')
INSERT [dbo].[RegularCustomer] ([RegularCustomerId], [RegularCustomerCnic], [RegularCustomerFirstName], [RegularCustomerLastName], [RegularCustomerPhoneNumber], [RegularCustomerCity], [RegularCustomerAddress]) VALUES (2, N'3420115566975', N'Muhammad Arslan', N'Tufail', N'03153428882', N'Gujrat', N'Aziz Abad')
INSERT [dbo].[RegularCustomer] ([RegularCustomerId], [RegularCustomerCnic], [RegularCustomerFirstName], [RegularCustomerLastName], [RegularCustomerPhoneNumber], [RegularCustomerCity], [RegularCustomerAddress]) VALUES (3, N'3420115566952', N'Muhammad', N'Imran', N'34201555182', N'Lahore', N'Aziz Chowk')
INSERT [dbo].[RegularCustomer] ([RegularCustomerId], [RegularCustomerCnic], [RegularCustomerFirstName], [RegularCustomerLastName], [RegularCustomerPhoneNumber], [RegularCustomerCity], [RegularCustomerAddress]) VALUES (4, N'3420115556678', N'Jawwad', N'Ali', N'03045094027', N'Isb', N'Chowk')
INSERT [dbo].[RegularCustomer] ([RegularCustomerId], [RegularCustomerCnic], [RegularCustomerFirstName], [RegularCustomerLastName], [RegularCustomerPhoneNumber], [RegularCustomerCity], [RegularCustomerAddress]) VALUES (5, N'656322212152', N'Muhammad', N'Hamza', N'03153422221', N'Isb', N'Chowk')
INSERT [dbo].[RegularCustomer] ([RegularCustomerId], [RegularCustomerCnic], [RegularCustomerFirstName], [RegularCustomerLastName], [RegularCustomerPhoneNumber], [RegularCustomerCity], [RegularCustomerAddress]) VALUES (6, N'3420115556697', N'Abdul', N'Wahab', N'03076215203', N'Gujrat', N'Aziz Abad')
INSERT [dbo].[RegularCustomer] ([RegularCustomerId], [RegularCustomerCnic], [RegularCustomerFirstName], [RegularCustomerLastName], [RegularCustomerPhoneNumber], [RegularCustomerCity], [RegularCustomerAddress]) VALUES (7, N'3420115566975', N'Muhammad Abdul', N'Wahab Tufail', N'03131313129', N'Gujrat', N'Sargodha Road Gujrat')
INSERT [dbo].[RegularCustomer] ([RegularCustomerId], [RegularCustomerCnic], [RegularCustomerFirstName], [RegularCustomerLastName], [RegularCustomerPhoneNumber], [RegularCustomerCity], [RegularCustomerAddress]) VALUES (8, N'342101212521', N'Rana ', N'Muazzam', N'03020202020', N'Shaiwal', N'Lahore')
INSERT [dbo].[RegularCustomer] ([RegularCustomerId], [RegularCustomerCnic], [RegularCustomerFirstName], [RegularCustomerLastName], [RegularCustomerPhoneNumber], [RegularCustomerCity], [RegularCustomerAddress]) VALUES (9, N'2541698745563', N'Sheheyar', N'Ahmad', N'03016224446', N'IsB', N'Isb')
INSERT [dbo].[RegularCustomer] ([RegularCustomerId], [RegularCustomerCnic], [RegularCustomerFirstName], [RegularCustomerLastName], [RegularCustomerPhoneNumber], [RegularCustomerCity], [RegularCustomerAddress]) VALUES (10, N'34232', N'Arslan', N'Tufail', N'0312222222', N'gujrat', N'gujrat')
INSERT [dbo].[RegularCustomer] ([RegularCustomerId], [RegularCustomerCnic], [RegularCustomerFirstName], [RegularCustomerLastName], [RegularCustomerPhoneNumber], [RegularCustomerCity], [RegularCustomerAddress]) VALUES (11, N'24121', N'Sir ', N'Asim', N'0302', N'Lhr', N'Lahore')
INSERT [dbo].[RegularCustomer] ([RegularCustomerId], [RegularCustomerCnic], [RegularCustomerFirstName], [RegularCustomerLastName], [RegularCustomerPhoneNumber], [RegularCustomerCity], [RegularCustomerAddress]) VALUES (12, N'112121', N'Rana ', N'Sab', N'03000', N'Lhr', N'Lahore')
INSERT [dbo].[RegularCustomer] ([RegularCustomerId], [RegularCustomerCnic], [RegularCustomerFirstName], [RegularCustomerLastName], [RegularCustomerPhoneNumber], [RegularCustomerCity], [RegularCustomerAddress]) VALUES (1010, N'54554', N'Faizan', N'sa,', N'121122100', N'asl', N'assa')
INSERT [dbo].[RegularCustomer] ([RegularCustomerId], [RegularCustomerCnic], [RegularCustomerFirstName], [RegularCustomerLastName], [RegularCustomerPhoneNumber], [RegularCustomerCity], [RegularCustomerAddress]) VALUES (2010, N'3420115566975', N'Haji Muhammad Arslan', N'Tufail Waria', N'03201201201', N'Gujrat', N'Mohallah Aziz Abad')
SET IDENTITY_INSERT [dbo].[RegularCustomer] OFF
SET IDENTITY_INSERT [dbo].[Sales] ON 

INSERT [dbo].[Sales] ([BillId], [CustomerPhone], [TotalAmount], [CustomerName], [Discount], [Items], [SaleDate]) VALUES (1, N'03076215203', 264, N'Regular Customer', 0, N'12 * BedSheet - 10 * Jacket - ', N'Monday, September 24, 2018')
INSERT [dbo].[Sales] ([BillId], [CustomerPhone], [TotalAmount], [CustomerName], [Discount], [Items], [SaleDate]) VALUES (2, N'03153428882', 728, N'Waria Bhai', 3, N'1 * BedSheet - 3 * Jacket - ', N'Monday, September 24, 2018')
INSERT [dbo].[Sales] ([BillId], [CustomerPhone], [TotalAmount], [CustomerName], [Discount], [Items], [SaleDate]) VALUES (3, N'03152021252', 1180, N'Adeel Bhai', 41, N'10 * Jacket - ', N'Monday, September 24, 2018')
INSERT [dbo].[Sales] ([BillId], [CustomerPhone], [TotalAmount], [CustomerName], [Discount], [Items], [SaleDate]) VALUES (4, N'03045094027', 2100, N'Regular Customer', 0, N'1 * BedSheet - 41 * Jacket - ', N'Sunday, September 23, 2018')
INSERT [dbo].[Sales] ([BillId], [CustomerPhone], [TotalAmount], [CustomerName], [Discount], [Items], [SaleDate]) VALUES (5, N'0321212121', 1584, N'Imran', 12, N'12 * BedSheet - ', N'Monday, September 24, 2018')
INSERT [dbo].[Sales] ([BillId], [CustomerPhone], [TotalAmount], [CustomerName], [Discount], [Items], [SaleDate]) VALUES (6, N'03153428882', 3234, N'Arslan', 2, N'22 * BedSheet - ', N'Monday, September 24, 2018')
INSERT [dbo].[Sales] ([BillId], [CustomerPhone], [TotalAmount], [CustomerName], [Discount], [Items], [SaleDate]) VALUES (7, N'03212212512', 2541, N'Nawaz', 23, N'22 * BedSheet - ', N'Monday, September 24, 2018')
INSERT [dbo].[Sales] ([BillId], [CustomerPhone], [TotalAmount], [CustomerName], [Discount], [Items], [SaleDate]) VALUES (8, N'03212121211', 368, N'Muhammad Arslan', 33, N'1 * BedSheet - 2 * Jacket - ', N'Monday, September 24, 2018')
INSERT [dbo].[Sales] ([BillId], [CustomerPhone], [TotalAmount], [CustomerName], [Discount], [Items], [SaleDate]) VALUES (9, N'03210210000', 7552, N'Ali Raza', 41, N'11 * BedSheet - 22 * Jacket - 45 * cotton suit - ', N'Monday, September 24, 2018')
INSERT [dbo].[Sales] ([BillId], [CustomerPhone], [TotalAmount], [CustomerName], [Discount], [Items], [SaleDate]) VALUES (10, N'03045094027', 2750, N'Regular Customer', 0, N'33 * Jacket - 22 * cotton suit - ', N'Monday, September 24, 2018')
INSERT [dbo].[Sales] ([BillId], [CustomerPhone], [TotalAmount], [CustomerName], [Discount], [Items], [SaleDate]) VALUES (11, N'03029752207', 25, N'Regular Customer', 0, N'1 * BedSheet - ', N'Sunday, September 23, 2018')
INSERT [dbo].[Sales] ([BillId], [CustomerPhone], [TotalAmount], [CustomerName], [Discount], [Items], [SaleDate]) VALUES (12, N'03029752207', 75, N'Regular Customer', 0, N'1 * BedSheet - 2 * Jacket - ', N'Monday, September 24, 2018')
INSERT [dbo].[Sales] ([BillId], [CustomerPhone], [TotalAmount], [CustomerName], [Discount], [Items], [SaleDate]) VALUES (13, N'03045094027', 2250, N'Regular Customer', 0, N'22 * BedSheet - 21 * Jacket - 2 * Jean - ', N'Monday, September 24, 2018')
INSERT [dbo].[Sales] ([BillId], [CustomerPhone], [TotalAmount], [CustomerName], [Discount], [Items], [SaleDate]) VALUES (14, N'03045094027', 600, N'Regular Customer', 0, N'12 * Jacket - ', N'Monday, September 24, 2018')
INSERT [dbo].[Sales] ([BillId], [CustomerPhone], [TotalAmount], [CustomerName], [Discount], [Items], [SaleDate]) VALUES (15, N'03029752207', 525, N'Regular Customer', 0, N'21 * BedSheet - ', N'Monday, September 24, 2018')
INSERT [dbo].[Sales] ([BillId], [CustomerPhone], [TotalAmount], [CustomerName], [Discount], [Items], [SaleDate]) VALUES (16, N'03121212121', 4288, N'Muhammad ', 33, N'12 * BedSheet - 23 * Jacket - ', N'Monday, September 24, 2018')
INSERT [dbo].[Sales] ([BillId], [CustomerPhone], [TotalAmount], [CustomerName], [Discount], [Items], [SaleDate]) VALUES (17, N'03020202021', 3276, N'faizan', 22, N'21 * Jacket - ', N'Monday, September 24, 2018')
INSERT [dbo].[Sales] ([BillId], [CustomerPhone], [TotalAmount], [CustomerName], [Discount], [Items], [SaleDate]) VALUES (18, N'03029752207', 5550, N'Regular Customer', 0, N'222 * BedSheet - ', N'Monday, September 24, 2018')
INSERT [dbo].[Sales] ([BillId], [CustomerPhone], [TotalAmount], [CustomerName], [Discount], [Items], [SaleDate]) VALUES (19, N'03029752207', 825, N'Regular Customer', 0, N'12 * Jacket - 10 * Jean - 11 * Shirt - ', N'Monday, September 24, 2018')
INSERT [dbo].[Sales] ([BillId], [CustomerPhone], [TotalAmount], [CustomerName], [Discount], [Items], [SaleDate]) VALUES (20, N'03131313129', 9000, N'Regular Customer', 0, N'12 * BedSheet - 12 * Jean - 55 * Shirt - 11 * Touser - 10 * Nicki - 100 * Cotton Pant - ', N'Tuesday, September 25, 2018')
INSERT [dbo].[Sales] ([BillId], [CustomerPhone], [TotalAmount], [CustomerName], [Discount], [Items], [SaleDate]) VALUES (21, N'03020202020', 15756, N'Regular Customer', 0, N'1212 * Jacket - 1 * Jean - 100 * Shirt - ', N'Tuesday, September 25, 2018')
INSERT [dbo].[Sales] ([BillId], [CustomerPhone], [TotalAmount], [CustomerName], [Discount], [Items], [SaleDate]) VALUES (22, N'0301010101', 1172, N'Muhammad Jawad', 33, N'11 * Jean - 11 * Shirt - 1 * Touser - 11 * Nicki - ', N'Tuesday, September 25, 2018')
INSERT [dbo].[Sales] ([BillId], [CustomerPhone], [TotalAmount], [CustomerName], [Discount], [Items], [SaleDate]) VALUES (23, N'03016224446', 130, N'Regular Customer', 0, N'12 * BedSheet - 1 * Jacket - ', N'Tuesday, September 25, 2018')
INSERT [dbo].[Sales] ([BillId], [CustomerPhone], [TotalAmount], [CustomerName], [Discount], [Items], [SaleDate]) VALUES (24, N'0312222222', 3172, N'Regular Customer', 0, N'1 * BedSheet - 22 * Jacket - 111 * Jean - 10 * Touser - 100 * Nicki - ', N'Wednesday, September 26, 2018')
INSERT [dbo].[Sales] ([BillId], [CustomerPhone], [TotalAmount], [CustomerName], [Discount], [Items], [SaleDate]) VALUES (25, N'0302', 12440, N'Regular Customer', 0, N'022 * BedSheet - 222 * Jacket - 1000 * cotton suit - ', N'Wednesday, September 26, 2018')
INSERT [dbo].[Sales] ([BillId], [CustomerPhone], [TotalAmount], [CustomerName], [Discount], [Items], [SaleDate]) VALUES (26, N'0301', 9583, N'Abc', 12, N'11 * BedSheet - 120 * Jean - ', N'Wednesday, September 26, 2018')
INSERT [dbo].[Sales] ([BillId], [CustomerPhone], [TotalAmount], [CustomerName], [Discount], [Items], [SaleDate]) VALUES (27, N'03000', 1729, N'Regular Customer', 0, N'12 * Jacket - 11 * Jean - 10 * Touser - 100 * Nicki - ', N'Thursday, September 27, 2018')
INSERT [dbo].[Sales] ([BillId], [CustomerPhone], [TotalAmount], [CustomerName], [Discount], [Items], [SaleDate]) VALUES (1024, N'03029752207', 1100, N'Regular Customer', 0, N'22 * BedSheet - 22 * Jacket - ', N'Monday, October 29, 2018')
INSERT [dbo].[Sales] ([BillId], [CustomerPhone], [TotalAmount], [CustomerName], [Discount], [Items], [SaleDate]) VALUES (2024, N'03020202020', 264, N'Regular Customer', 0, N'22 * Cotton Jean - ', N'Thursday, November 8, 2018')
INSERT [dbo].[Sales] ([BillId], [CustomerPhone], [TotalAmount], [CustomerName], [Discount], [Items], [SaleDate]) VALUES (2025, N'03153428882', 675, N'Adeel', 10, N'5 * BedSheet - ', N'Tuesday, November 20, 2018')
INSERT [dbo].[Sales] ([BillId], [CustomerPhone], [TotalAmount], [CustomerName], [Discount], [Items], [SaleDate]) VALUES (2026, N'251001201521', 294, N'as', 2, N'2 * BedSheet - ', N'Wednesday, November 14, 2018')
INSERT [dbo].[Sales] ([BillId], [CustomerPhone], [TotalAmount], [CustomerName], [Discount], [Items], [SaleDate]) VALUES (2027, N'122112', 132, N'a,', 12, N'1 * BedSheet - ', N'Wednesday, November 14, 2018')
INSERT [dbo].[Sales] ([BillId], [CustomerPhone], [TotalAmount], [CustomerName], [Discount], [Items], [SaleDate]) VALUES (2028, N'03153428882', 3150, N'Arslan', 0, N'12 * BedSheet - 12 * BedSheet - 1 * BedSheet - 21 * BedSheet - 21 * BedSheet - ', N'Wednesday, November 21, 2018')
INSERT [dbo].[Sales] ([BillId], [CustomerPhone], [TotalAmount], [CustomerName], [Discount], [Items], [SaleDate]) VALUES (2029, N'021212212', 300, N'adeel', 0, N'2 * BedSheet - ', N'Wednesday, November 28, 2018')
INSERT [dbo].[Sales] ([BillId], [CustomerPhone], [TotalAmount], [CustomerName], [Discount], [Items], [SaleDate]) VALUES (2030, N'0212521', 300, N'Adeel', 0, N'2 * BedSheet - ', N'Wednesday, November 21, 2018')
INSERT [dbo].[Sales] ([BillId], [CustomerPhone], [TotalAmount], [CustomerName], [Discount], [Items], [SaleDate]) VALUES (2031, N'03029752207', 297, N'adeel', 1, N'2 * BedSheet - ', N'Thursday, November 15, 2018')
INSERT [dbo].[Sales] ([BillId], [CustomerPhone], [TotalAmount], [CustomerName], [Discount], [Items], [SaleDate]) VALUES (2032, N'03020202020', 48, N'Regular Customer', 0, N'2 * BedSheet - 2 * Jacket - ', N'Thursday, November 8, 2018')
INSERT [dbo].[Sales] ([BillId], [CustomerPhone], [TotalAmount], [CustomerName], [Discount], [Items], [SaleDate]) VALUES (2033, N'03153428882', 4095, N'Muhammad Arslan Tufail Waria', 10, N'1 * BedSheet - 22 * Jacket - ', N'')
INSERT [dbo].[Sales] ([BillId], [CustomerPhone], [TotalAmount], [CustomerName], [Discount], [Items], [SaleDate]) VALUES (2034, N'03153428882', 4059, N'Muhammad Arslan Tufail Waria', 10, N'022 * BedSheet - 10 * Touser - 100 * Nicki - ', N'11/8/2018 12:00:00 AM')
INSERT [dbo].[Sales] ([BillId], [CustomerPhone], [TotalAmount], [CustomerName], [Discount], [Items], [SaleDate]) VALUES (2035, N'03152222222', 1350, N'Adeel', 10, N'10 * BedSheet - ', N'11/8/2018')
INSERT [dbo].[Sales] ([BillId], [CustomerPhone], [TotalAmount], [CustomerName], [Discount], [Items], [SaleDate]) VALUES (2036, N'03153428882', 297, N'Arslan', 1, N'10 * White Shirt - ', N'11/8/2018')
INSERT [dbo].[Sales] ([BillId], [CustomerPhone], [TotalAmount], [CustomerName], [Discount], [Items], [SaleDate]) VALUES (2037, N'
41114444111', 2298, N'ksj', 1, N'211 * Nicki - ', N'11/8/2018 12:00:00 AM')
INSERT [dbo].[Sales] ([BillId], [CustomerPhone], [TotalAmount], [CustomerName], [Discount], [Items], [SaleDate]) VALUES (2038, N'03153428882', 900, N'arslan', 10, N'5 * Jacket - ', N'Thursday, November 8, 2018')
INSERT [dbo].[Sales] ([BillId], [CustomerPhone], [TotalAmount], [CustomerName], [Discount], [Items], [SaleDate]) VALUES (2039, N'02111212121', 1485, N'Muhammad', 1, N'10 * BedSheet - ', N'11/8/2018 12:00:00 AM')
INSERT [dbo].[Sales] ([BillId], [CustomerPhone], [TotalAmount], [CustomerName], [Discount], [Items], [SaleDate]) VALUES (2040, N'00002122112', 14850, N'Arslan', 1, N'100 * BedSheet - ', N'11/8/2018 12:00:00 AM')
INSERT [dbo].[Sales] ([BillId], [CustomerPhone], [TotalAmount], [CustomerName], [Discount], [Items], [SaleDate]) VALUES (2041, N'0000022122', 135000, N'Adeel', 10, N'1000 * cotton suit - ', N'11/8/2018')
INSERT [dbo].[Sales] ([BillId], [CustomerPhone], [TotalAmount], [CustomerName], [Discount], [Items], [SaleDate]) VALUES (2042, N'00012012000', 1111, N'Haji Sab', 1, N'100 * Touser - 2 * Nicki - ', N'11/8/2018')
INSERT [dbo].[Sales] ([BillId], [CustomerPhone], [TotalAmount], [CustomerName], [Discount], [Items], [SaleDate]) VALUES (2043, N'0312222222', 260, N'Regular Customer', 0, N'10 * BedSheet - 10 * Jacket - ', N'11/8/2018')
INSERT [dbo].[Sales] ([BillId], [CustomerPhone], [TotalAmount], [CustomerName], [Discount], [Items], [SaleDate]) VALUES (2044, N'03131313129', 10980, N'Regular Customer', 0, N'122 * BedSheet - 122 * Jacket - ', N'11/8/2018')
INSERT [dbo].[Sales] ([BillId], [CustomerPhone], [TotalAmount], [CustomerName], [Discount], [Items], [SaleDate]) VALUES (2045, N'03201201201', 44400, N'Regular Customer', 0, N'100 * BedSheet - 111 * Jacket - 222 * Jean - 10 * White Shirt - 1 * Round Neck Shirt - ', N'11/8/2018')
SET IDENTITY_INSERT [dbo].[Sales] OFF
/****** Object:  StoredProcedure [dbo].[sp_DeleteItem]    Script Date: 1/11/2019 5:04:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_DeleteItem]
@ItemId int 
AS
BEGIN
	DELETE FROM Item 
	WHERE ItemId = @ItemId
END


GO
/****** Object:  StoredProcedure [dbo].[sp_GetBillNumber]    Script Date: 1/11/2019 5:04:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_GetBillNumber]
AS
BEGIN
	SELECT top 1 (BillId)
	FROM Sales 
	order by BillId desc
END

SELECT * FROM Sales

GO
/****** Object:  StoredProcedure [dbo].[sp_GetByDateHistory]    Script Date: 1/11/2019 5:04:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_GetByDateHistory]
@SaleDate varchar(max)
AS
BEGIN
	SELECT * 
	FROM Sales
	WHERE SaleDate BETWEEN @SaleDate AND @SaleDate
	
END
GO
/****** Object:  StoredProcedure [dbo].[sp_GetByNameHistory]    Script Date: 1/11/2019 5:04:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_GetByNameHistory]
@BillId varchar(max)
AS
BEGIN
	SELECT * FROM Sales
	WHERE BillId = @BillId
END


GO
/****** Object:  StoredProcedure [dbo].[sp_GetCustomerData]    Script Date: 1/11/2019 5:04:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_GetCustomerData]
@RegularCustomerPhoneNumber varchar(13)
AS
BEGIN
	SELECT * 
	FROM RegularCustomer
	Where RegularCustomerPhoneNumber = @RegularCustomerPhoneNumber
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetItemData]    Script Date: 1/11/2019 5:04:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_GetItemData]
AS
BEGIN
	SELECT * FROM Item
END


GO
/****** Object:  StoredProcedure [dbo].[sp_GetPhoneHistory]    Script Date: 1/11/2019 5:04:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_GetPhoneHistory]
@CustomerPhone varchar(13)
AS
BEGIN
	SELECT * FROM Sales
	WHERE CustomerPhone = @CustomerPhone
END


GO
/****** Object:  StoredProcedure [dbo].[sp_GetRecord]    Script Date: 1/11/2019 5:04:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_GetRecord]
@ItemName varchar(30),
@ItemId int
AS
BEGIN
	SELECT I.ItemName FROM Item AS I
	WHERE ItemName = @ItemName OR I.ItemId = @ItemId
	END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetRegularCustomer]    Script Date: 1/11/2019 5:04:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_GetRegularCustomer]
@CustomerId int
AS
BEGIN
	SELECT RC.RegularCustomerPhoneNumber,PP.PackagesPricePerPiece,Rc.RegularCustomerId,Rc.RegularCustomerFirstName , Rc.RegularCustomerLastName
	FROM RegularCustomer as RC
	INNER JOIN Packages as PP
	On RC.RegularCustomerId = pp.CustomerId

	where Rc.RegularCustomerId = @CustomerId
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetRegularCustomerRecord]    Script Date: 1/11/2019 5:04:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_GetRegularCustomerRecord]
AS
BEGIN
	SELECT * FROM Item
END



GO
/****** Object:  StoredProcedure [dbo].[sp_InsertItem]    Script Date: 1/11/2019 5:04:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_InsertItem]
@ItemName varchar(30), 
@ItemPrice int
AS
BEGIN
	INSERT INTO Item(ItemName, ItemPrice)
	VALUES(@ItemName,@ItemPrice)
END


GO
/****** Object:  StoredProcedure [dbo].[sp_Login]    Script Date: 1/11/2019 5:04:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Login]
@LoginUserName varchar(50),
@LoginPassword varchar(15)
AS
BEGIN
	
	SELECT LoginUserName,LoginPassword from [Login] as L
	Where L.LoginUserName=@LoginUserName AND L.LoginPassword=@LoginPassword
END

GO
/****** Object:  StoredProcedure [dbo].[sp_NewCustomer]    Script Date: 1/11/2019 5:04:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_NewCustomer]
@RegularCustomerCnic varchar(20), 
@RegularCustomerFirstName varchar(25), 
@RegularCustomerLastName varchar(20), 
@RegularCustomerPhoneNumber varchar(13),
@RegularCustomerCity varchar(20), 
@RegularCustomerAddress varchar(max)
AS
BEGIN
	INSERT INTO RegularCustomer(RegularCustomerCnic, RegularCustomerFirstName, RegularCustomerLastName, RegularCustomerPhoneNumber, RegularCustomerCity, RegularCustomerAddress)
	VALUES(@RegularCustomerCnic,@RegularCustomerFirstName,@RegularCustomerLastName,@RegularCustomerPhoneNumber,@RegularCustomerCity,@RegularCustomerAddress)
END

GO
/****** Object:  StoredProcedure [dbo].[sp_SavePackage]    Script Date: 1/11/2019 5:04:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_SavePackage]
@CustomerId int, 
@PackagesPricePerPiece int
AS
BEGIN
	INSERT INTO Packages(CustomerId, PackagesPricePerPiece)
	VALUES(@CustomerId,@PackagesPricePerPiece)
END

GO
/****** Object:  StoredProcedure [dbo].[sp_SaveRegularCustomerRecord]    Script Date: 1/11/2019 5:04:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_SaveRegularCustomerRecord]
@CustomerPhone varchar(13),
@TotalAmount int,
@Items varchar(max),
@SaleDate varchar(max)
AS
BEGIN
	INSERT INTO Sales(CustomerPhone, TotalAmount, CustomerName, Discount, Items,SaleDate)
	VALUES(@CustomerPhone, @TotalAmount,'Regular Customer' ,0,@Items,@SaleDate)
END



GO
/****** Object:  StoredProcedure [dbo].[sp_SaveSales]    Script Date: 1/11/2019 5:04:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_SaveSales]
@Discount int,
@CustomerPhone varchar(13), 
@TotalAmount int, 
@CustomerName varchar(50),
@Items varchar(max),
@SaleDate varchar(max)

AS
BEGIN
	INSERT INTO Sales(CustomerPhone, TotalAmount, CustomerName, Discount,Items,SaleDate)
	VALUES(@CustomerPhone,@TotalAmount,@CustomerName,@Discount,@Items,@SaleDate)

END

GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateItem]    Script Date: 1/11/2019 5:04:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_UpdateItem]
@ItemId int,
@ItemName varchar(30), 
@ItemPrice int
AS
BEGIN
	UPDATE Item
	SET ItemName=@ItemName , ItemPrice=@ItemPrice
	WHERE ItemId=@ItemId
END


GO
/****** Object:  StoredProcedure [dbo].[sp_UpdatePassword]    Script Date: 1/11/2019 5:04:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_UpdatePassword]
@LoginPassword varchar(50)
AS
BEGIN
	UPDATE Login
	SET LoginPassword=@LoginPassword
END

GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateUserName]    Script Date: 1/11/2019 5:04:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_UpdateUserName]
@LoginUserName varchar(50)
AS
BEGIN
	UPDATE Login
	SET LoginUserName=@LoginUserName
END

GO
